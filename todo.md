# To do List
- create a method for the 3 highly rated restaurants ***[done]***
- Remember to put a time of arrival for bookings ***[done]***

- how can someone create a mail server because who is supposed to create one for sending mails  ***[needs domain and server set up first or we can use sendgrid which isn't free]***

- You need to create a search method like in restaurants whereby you can - search for a persons restaurant using geo-coordinates and then you send me the 5 nearest restaurants to that person as his location that i will have sent to you, send me the JSON data for restaurants to display on app like this  ***[done]***

```json
 [
    {
        "id":1,
        "name": "Rixos The Palm Dubai",
        "location": {
            latitude: 25.1212,
            longitude: 55.1535,
        },
    },
    {
        "id":1,
        "name": "Shangri-La Hotel",
        "location": {
            latitude: 25.2084,
            longitude: 55.2719, 
        },
    },
    {
        "id":1,
        "name": "Grand Hyatt",
        "location": {
            latitude: 25.2285,
            longitude: 55.3273, 
        },
    }
]

```
- I hope you have categorised restaurants in terms of thirsty, hungry and out&about because a person will click on a button of let's say hungry and on the maps it needs to show him only restaurants for hungry that near him based on his location ***[done]***

- when you create a restaurant, add a field of the restaurant's phone number ***[done]***

- when you send the details of a particular restaurant, kindly include a capacity field so that i display that ***[done]***


---

- time a restaurant booking with the order of the person ***[ will explain more on this ]***

- Create a method that show categorises restaurants into Hungry, thirsty and out&about. This is to be used when a person clicks on let's say thirsty it shows on the map the restaurants that are under category thirsty near his or her vicinity or coordinates. ***[done]***

- time a restaurant booking with the order of the person ***[done]***
- Create a method that show categorises restaurants into Hungry, thirsty and out&about. This is to be - used when a person clicks on let's say thirsty it shows on the map the restaurants that are under  ***[done]***
- category thirsty near his or her vicinity or coordinates. ***[done]***

---


- make method for showing the contacts of radi owners, more like contact us ***[done]***
- make a method for About Us and Privacy policy of the app ***[done]***
- make a method that shows the info of the app, version and anything else needed ***[done]***
- make the method for rating restaurants available ***[done]***
- booking time tracking methos on API returns difference of time now and time of booking ***[done]***
- make a method that shows the bookings of a person **[done]**

---






----
- All payments made to restaurant with category i.e (MPESA, Paypal, )
- all customer payments
- payment total within spsecified period ..the period specified should not be more that 90 days[you have to check for this]

    ```json

        {
            "start":"2019-06-14T12:47:15.846202Z",
            "end":"2019-06-14T12:47:15.846202Z",
            "restaurant_id":1,
        }

    ```
     - Expected output
     ```json

        {
            "id":1,
            "customer":{
                "phone":"25470148934",
                "name":"John Doe",
            },
            "billInfo":{
                "foods":{
                    //all foods purchased
                }
            },
            "amountPayed":"300",
            "modeOfPayment":"MPESA",
            "dateOfPayment":""
        }

    ```

- also all payments within a specified period my request will be as follows 

    ```json

        {
            "start":"2019-06-14T12:47:15.846202Z",
            "end":"2019-06-14T12:47:15.846202Z",
            "restaurant_id":1,
        }

    ```
    - Expected output
     ```json

        {
            "id":1,
            "customer":{
                "phone":"25470148934",
                "name":"John Doe",
            },
            "billInfo":{
                "foods":{
                    //all foods purchased
                }
            },
            "amountPayed":"300",
            "modeOfPayment":"MPESA",
            "dateOfPayment":""
        }

    ```
- once payment is done you should update bill status to PAID
- add a method by which a person can add a restaurant as his or her favorite restaurant 
- top 3 highly rated restaurants **[done]**
- how to send headers in js
    let headers = {
    
        accept: "application/json",

        "Accept-Language": "en-US,en;q=0.8",

        'Content-Type': 'application/json',

        "token": token
    
    }

------
- create a delete like method to be used a person unlikes a post 
- make method for Radi owners to be able to send messages to the app
- remember that the app will have advertisments so in the posts that appear in the app an advertisemnt for a hotel will appear like after 5 posts just like on instagram (i know i hadnt told you this earlier sorry)
- Create a method for restaurant offers 
- i hope you have made the booking method and aslo the booking table must return back the booking id so that i tie it with an order of a person
- Create a method that i can get the most current booking of a person
