from rest_framework.serializers import ModelSerializer
from .models import Config, RadiContact, RadiMessages, RadiPrivacy, RadiTermsConditions


class RadiContactSerializer(ModelSerializer):
    class Meta:
        model = RadiContact
        fields = '__all__'

class RadiMessagesSerializer(ModelSerializer):
    class Meta:
        model = RadiMessages
        fields = '__all__'


class RadiPrivacySerializer(ModelSerializer):
    class Meta:
        model = RadiPrivacy
        fields = '__all__'


class RadiTermsConditionsSerializer(ModelSerializer):
    class Meta:
        model = RadiTermsConditions
        fields = '__all__'