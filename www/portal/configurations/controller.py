from django.core.paginator import Paginator
from rest_framework.decorators import api_view
from rest_framework.response import Response
import datetime, pytz
from authentication.api.decorators import check_token_status
from .models import Config, RadiContact, RadiMessages, RadiPrivacy, RadiTermsConditions
from .serializers import RadiContactSerializer, RadiMessagesSerializer, RadiPrivacySerializer, RadiTermsConditionsSerializer


@api_view(['GET'])
def reset_config(request):
    """
    Reset All System Configurations to default
    """
    try:
        Config.truncate()
        _new_config = Config(id=1,
                             phone='254712345678',
                             email='contact@radi.com',
                             privacy_policy='text',
                             about_us='text',
                             active_app_version='1.0.0')
        _new_config.save()

        return Response({
            'data': {},
            'status_code': 200,
            'message': 'Successfully saved'
        })
    except BaseException as e:
        return Response({
            'data': {},
            'status_code': 500,
            'message': 'error ' + str(e)
        })


@api_view(['POST'])
@check_token_status()
def update_config(request):
    """
    Update System Configurations
    ----
        {
            phone : string
            email : string
            privacy_policy : string
            about_us : string
            active_app_version : string
        }
    """
    try:
        _conf = Config.objects.filter(id=1)
        _conf.phone = request.data['phone']
        _conf.email = request.data['email']
        _conf.privacy_policy = request.data['privacy_policy']
        _conf.about_us = request.data['about_us']
        _conf.active_app_version = request.data['active_app_version']
        _conf.save()
        return Response({
            'data': {},
            'status_code': 200,
            'message': 'Successfully saved'
        })
    except BaseException as e:
        return Response({
            'data': {},
            'status_code': 500,
            'message': 'error ' + str(e)
        })


class RadiContacts():
    # create contact details of radi app management      
    @api_view(['POST'])
    @check_token_status()
    def create_radi_contact(request):
        """
        Add contact details for Radi app
        -----
            {
                name:  Radi
                email: radi@gmail.com
                msisdn: 079362622
                box_office: 984-00100
                street: Moi avenue
                website: http://www.Radi.com
                town: Nairobi
                country: Kenya
            }
        """
        try:
            contact = RadiContact(
                name = request.data['name'],
                email = request.data['email'],
                msisdn = request.data['msisdn'],
                box_office = request.data['box_office'],
                street = request.data['street'],
                website = request.data['website'],
                town = request.data['town'],
                country = request.data['country'],
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC)
            )
            contact.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # Get most current radi management contact details
    @api_view(['GET'])
    def get_radi_contact_details(request):
        """
        Get the contact details of Radi App
        """
        try:
            contact = RadiContact.objects.latest('created_at')
            data = {
                'data': RadiContactSerializer(contact).data,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # delete a radi post for person
    @api_view(['DELETE'])
    @check_token_status()
    def delete_radi_contact_details(request):
        """
        Delete radi contact details
        -----
            {
                id:int
            }

        """
        try:
            delete = RadiContact.objects.filter(id=request.data['id']).delete()
            data = {
                "message": "Contact deleted",
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error'+str(e),
            }
            return Response(error)


    # update radi contact details
    @api_view(['POST'])
    @check_token_status()
    def update_radi_contact_details(request):
        """
        Update Post details
        -----
            {
                id: 1
                name:  Radi
                email: radi@gmail.com
                msisdn: 079362622
                box_office: 984-00100
                street: Moi avenue
                website: http://www.Radi.com
                town: Nairobi
                country: Kenya
            }
        """
        try:
            contact = RadiContact.objects.get(id=request.data['id'])
            contact.name = request.data['name']
            contact.email = request.data['email']
            contact.msisdn = request.data['msisdn']
            contact.box_office = request.data['box_office']
            contact.street = request.data['street']
            contact.website = request.data['website']
            contact.town = request.data['town']
            contact.country = request.data['country']
            contact.updated_at = datetime.datetime.now(tz=pytz.UTC)
            contact.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)





class RadiMessage():
    # create messages of radi app management      
    @api_view(['POST'])
    @check_token_status()
    def create_radi_message(request):
        """
        Message for Radi App
        -----
            {
                title:  Warning
                message: jdkjdsjsds
            }
        """
        try:
            message = RadiMessages(
                title = request.data['title'],
                message = request.data['message'],
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC)
            )
            message.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # Get messages for radi app
    @api_view(['POST'])
    @check_token_status()
    def get_radi_messages(request):
        """
        Get messages for radi app
        -----
            {
                page:  1
                items: 10
            }
        """
        try:
            messages = RadiMessages.objects.all()
            page = request.data['page']
            paginator = Paginator(messages, request.data['items'])
            details=[]
            for message in messages:
                details.append(RadiContactSerializer(message).data)
            data = {
                'data': details,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)

    # Get latest message for radi app
    @api_view(['POST'])
    @check_token_status()
    def get_radi_latest_message(request):
        """
        Get latest message from radi app
        """
        try:
            messages = RadiMessages.objects.latest('updated_at')
            data = {
                'data': RadiMessagesSerializer(messages).data,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # delete a radi message for app
    @api_view(['DELETE'])
    @check_token_status()
    def delete_radi_messages(request):
        """
        Delete radi message
        -----
            {
                id:1
            }

        """
        try:
            delete = RadiMessages.objects.filter(id=request.data['id']).delete()
            data = {
                "message": "Message deleted",
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error'+str(e),
            }
            return Response(error)


    # update radi message
    @api_view(['POST'])
    @check_token_status()
    def update_radi_message(request):
        """
        Update Post details
        -----
            {
                id: 1
                title:  adbad
                message: hajhadjad
            }
        """
        try:
            messages = RadiMessages.objects.get(id=request.data['id'])
            messages.title = request.data['title']
            messages.message = request.data['message']
            messages.updated_at = datetime.datetime.now(tz=pytz.UTC)
            messages.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)



class RadiPrivacies():
    # create privacy details of radi app management      
    @api_view(['POST'])
    @check_token_status()
    def create_radi_privacy(request):
        """
        Privacy details for Radi App
        -----
            {
                title:  Warning
                message: jdkjdsjsds
            }
        """
        try:
            message = RadiPrivacy(
                title = request.data['title'],
                message = request.data['message'],
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC)
            )
            message.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # Get privacy details for radi app
    @api_view(['POST'])
    def get_radi_privacy(request):
        """
        Get privacy details for radi app
        -----
            {
                page:  1
                items: 10
            }
        """
        try:
            messages = RadiPrivacy.objects.all()
            page = request.data['page']
            paginator = Paginator(messages, request.data['items'])
            details=[]
            for message in messages:
                details.append(RadiPrivacySerializer(message).data)
            data = {
                'data': details,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # Get latest privacy for radi app
    @api_view(['POST'])
    @check_token_status()
    def get_radi_latest_privacy(request):
        """
        Get latest privacy from radi app
        """
        try:
            messages = RadiPrivacy.objects.latest('updated_at')
            data = {
                'data': RadiPrivacySerializer(messages).data,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # delete a radi privacy details for person
    @api_view(['DELETE'])
    @check_token_status()
    def delete_radi_privacy(request):
        """
        Delete radi privacy details
        -----
            {
                id:1
            }

        """
        try:
            delete = RadiPrivacy.objects.filter(id=request.data['id']).delete()
            data = {
                "message": "Radi Privacy deleted",
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error'+str(e),
            }
            return Response(error)


    # update radi privacy details
    @api_view(['POST'])
    @check_token_status()
    def update_radi_privacy(request):
        """
        Update Radi Privacy details
        -----
            {
                id: 1
                title:  adbad
                message: hajhadjad
            }
        """
        try:
            messages = RadiPrivacy.objects.get(id=request.data['id'])
            messages.title = request.data['title']
            messages.message = request.data['message']
            messages.updated_at = datetime.datetime.now(tz=pytz.UTC)
            messages.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)



class RadiTermsCondition():
    # create radi terms and conditions of radi app management      
    @api_view(['POST'])
    @check_token_status()
    def create_radi_terms(request):
        """
        Terms and conditions for Radi App
        -----
            {
                title:  Warning
                message: jdkjdsjsds
            }
        """
        try:
            message = RadiTermsConditions(
                title = request.data['title'],
                message = request.data['message'],
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC)
            )
            message.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # Get terms and conditions for radi app
    @api_view(['POST'])
    def get_radi_terms(request):
        """
        Get terms and conditions for radi app
        -----
            {
                page:  1
                items: 10
            }
        """
        try:
            messages = RadiTermsConditions.objects.all()
            page = request.data['page']
            paginator = Paginator(messages, request.data['items'])
            details=[]
            for message in paginator:
                details.append(RadiTermsConditionsSerializer(message).data)
            data = {
                'data': details,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # Get latest terms and conditions for radi app
    @api_view(['POST'])
    @check_token_status()
    def get_radi_latest_terms(request):
        """
        Get latest terms and conditions from radi app
        """
        try:
            messages = RadiTermsConditions.objects.latest('updated_at')
            data = {
                'data': RadiTermsConditionsSerializer(messages).data,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)


    # delete a radi terms and conditions for person
    @api_view(['DELETE'])
    @check_token_status()
    def delete_radi_terms(request):
        """
        Delete radi terms and conditions
        -----
            {
                id:1
            }

        """
        try:
            delete = RadiTermsConditions.objects.filter(id=request.data['id']).delete()
            data = {
                "message": "Radi terms and conditions deleted",
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error'+str(e),
            }
            return Response(error)


    # update radi terms and conditions
    @api_view(['POST'])
    @check_token_status()
    def update_radi_terms(request):
        """
        Update Radi terms and conditions
        -----
            {
                id: 1
                title:  adbad
                message: hajhadjad
            }
        """
        try:
            messages = RadiTermsConditions.objects.get(id=request.data['id'])
            messages.title = request.data['title']
            messages.message = request.data['message']
            messages.updated_at = datetime.datetime.now(tz=pytz.UTC)
            messages.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
            }
            return Response(error)



