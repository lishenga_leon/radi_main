from django.urls import include, path
from . import controller

urlpatterns = [

    path('config/reset', controller.reset_config),
    path('config/update', controller.update_config),

    #RadiContact routes
    path('RadiContact/createRadiContact/', controller.RadiContacts.create_radi_contact),
    path('RadiContact/getRadiContact/', controller.RadiContacts.get_radi_contact_details),
    path('RadiContact/deleteRadiContact/', controller.RadiContacts.delete_radi_contact_details),
    path('RadiContact/updateRadiContact/', controller.RadiContacts.update_radi_contact_details),


    #RadiMessages routes
    path('RadiMessages/createRadiMessages/', controller.RadiMessage.create_radi_message),
    path('RadiMessages/getRadiMessages/', controller.RadiMessage.get_radi_messages),
    path('RadiMessages/deleteRadiMessages/', controller.RadiMessage.delete_radi_messages),
    path('RadiMessages/updateRadiMessages/', controller.RadiMessage.update_radi_message),
    path('RadiMessages/getRadiMessagesApp/', controller.RadiMessage.get_radi_latest_message),


    #RadiPrivacy routes
    path('RadiPrivacy/createRadiPrivacy/', controller.RadiPrivacies.create_radi_privacy),
    path('RadiPrivacy/getRadiPrivacy/', controller.RadiPrivacies.get_radi_privacy),
    path('RadiPrivacy/deleteRadiPrivacy/', controller.RadiPrivacies.delete_radi_privacy),
    path('RadiPrivacy/updateRadiPrivacy/', controller.RadiPrivacies.update_radi_privacy),
    path('RadiPrivacy/getRadiPrivacyApp/', controller.RadiPrivacies.get_radi_latest_privacy),


    #RadiPrivacy routes
    path('RadiTermsConditions/createRadiTermsConditions/', controller.RadiTermsCondition.create_radi_terms),
    path('RadiTermsConditions/getRadiTermsConditions/', controller.RadiTermsCondition.get_radi_terms),
    path('RadiTermsConditions/deleteRadiTermsConditions/', controller.RadiTermsCondition.delete_radi_terms),
    path('RadiTermsConditions/updateRadiTermsConditions/', controller.RadiTermsCondition.update_radi_terms),
    path('RadiTermsConditions/getRadiTermsConditionsApp/', controller.RadiTermsCondition.get_radi_latest_terms),


    # Adverts routes
    # path('', include('adverts.urls')),
]
