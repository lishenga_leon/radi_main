from django.db import models, connection

# Create your models here.


class Config(models.Model):
    id = models.AutoField(primary_key=True)
    phone = models.CharField(max_length=255, default=None, null=True)
    email = models.CharField(max_length=255, default=None, null=True)
    privacy_policy = models.TextField(default=None, null=True)
    about_us = models.TextField(default=None, null=True)
    active_app_version = models.CharField(
        max_length=255, default=None, null=True)

    @classmethod
    def truncate(cls):
        with connection.cursor() as cursor:
            cursor.execute(
                'TRUNCATE TABLE "{0}" CASCADE'.format(cls._meta.db_table))


# class for contact details of Radi App
class RadiContact(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, default=None)
    email = models.CharField(max_length=255, default=None)
    msisdn = models.CharField(max_length=255, default=None)
    box_office = models.CharField(max_length=255, default=None)
    street = models.CharField(max_length=255, default=None)
    website = models.CharField(max_length=255, default=None)
    town = models.CharField(max_length=255, default=None)
    country = models.CharField(max_length=255, default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)


# class for messages of Radi App
class RadiMessages(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, default=None)
    message = models.CharField(max_length=10000, default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

# class for privacy details of Radi App
class RadiPrivacy(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, default=None)
    message = models.CharField(max_length=10000, default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

# class for terms and conditions of Radi App
class RadiTermsConditions(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, default=None)
    message = models.CharField(max_length=10000, default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)


