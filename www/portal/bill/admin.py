from django.contrib import admin
from bill.models import Order, Bill

# Register your models here.
admin.site.register(Bill)
admin.site.register(Order)
