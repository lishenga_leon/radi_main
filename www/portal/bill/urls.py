from django.urls import include, path
from . import controller

urlpatterns = [
    path('orders/create', controller.orders.create),
    path('orders/getActive', controller.orders.get_active_orders),
    path('orders/getPersonsOrders', controller.orders.get_person_orders),
    path('orders/getTimetoOrder', controller.orders.get_booking_time_difference),
    path('orders/getActiveCount', controller.orders.get_total_active_orders),
    path('orders/serveOrder', controller.orders.approve_order),

    path('', include('analytics.urls')),
]
