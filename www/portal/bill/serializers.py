from rest_framework.serializers import ModelSerializer, Serializer, SerializerMethodField
from .models import Order, OrderDetails
from food.serializers import FoodieSerializer


class OrdersSerializers(ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

class OrderDetailsSerializers(ModelSerializer):
    class Meta:
        model = OrderDetails
        fields = '__all__'
