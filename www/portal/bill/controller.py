import datetime
from pprint import pprint
import json
import pytz
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core.paginator import Paginator
from django.utils.dateparse import parse_datetime
from authentication.api.decorators import check_token_status


from bill.models import Order, OrderDetails
from food.models import Foodie
from person.models import person
from bill.serializers import OrdersSerializers
from restaurant.models import Restaurant
# from datetime import datetime


class bills():
    """
    create bill
    -----
        {
           order_id
        }

    """
    @api_view(['POST'])
    def create(request):
        pass


class orders():

    @api_view(['POST'])
    @check_token_status()
    def approve_order(request):
        """
        Approve Order
        --------
            {
                order_id:int,
            }

        """
        data = {}
        try:
            order = Order.objects.get(id=request.data['order_id'])
            order.status = 'served'
            order.save()
            data = {
                "data": {},
                "message": 'Order Served',
                "status_code": 200
            }
        except BaseException as e:
            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }
        return Response(data)


    @api_view(['POST'])
    @check_token_status()
    def get_person_orders(request):
        """
        Get Persons Orders
        --------
            {
                person_id:int,
            }

        """
        data = {}
        val = []
        try:
            order = Order.objects.filter(
                person_id=request.data['person_id']).order_by('-expected_time')
            for ordering in order:
                det = {
                    'order': OrdersSerializers(ordering).data,
                    'content': ordering.details(),
                    'restaurant': ordering.Restaurantdetails()
                }
                val.append(det)
            data = {
                "data": val,
                "message": 'Orders Gotten',
                "status_code": 200
            }
        except BaseException as e:
            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }
        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def create(request):
        """
        This Method allows the user to create and be billed for a particular order
        The order may contain numerous data as shown
        -----
            {
                person_id: int,
                restaurant_id: int,
                expected_people: int,
                order_type: choice(TAKEOUT, EATIN) | optional
                expected_time: DateTime | 2019-05-21T09:39:49.834338Z
                foods:[
                    {
                        food_id:int,
                        quantity:1
                    },
                    {
                        food_id:int,
                        quantity:1
                    }
                ],
                instructions: ndkadakjdand
            }
        """
        data = {}
        try:
            guy = person.objects.get(id=request.data['person_id'])
            restaurant = Restaurant.objects.get(
                id=request.data['restaurant_id'])
            food_data = request.data['foods']
            order = Order(
                person_id=request.data['person_id'], 
                food_instructions = request.data['instructions'],
                restaurant=restaurant, 
                created_at=datetime.datetime.now(tz=pytz.timezone('Africa/Nairobi')),
                updated_at=datetime.datetime.now(tz=pytz.timezone('Africa/Nairobi'))
            )

            """

            """
            order.status = "pending"
            order.order_type = request.data['order_type']
            order.expected_time = request.data['expected_time']

            order.save()

            for food in food_data:
                foodie = Foodie.objects.get(id=food['food_id'])
                detail = OrderDetails(order=order, foodie=foodie, quantity=food['quantity'], created_at=datetime.datetime.now(tz=pytz.timezone('Africa/Nairobi')),
                                      updated_at=datetime.datetime.now(tz=pytz.timezone('Africa/Nairobi')))
                detail.save()

            data = {
                "data": {
                    'order': OrdersSerializers(order).data,
                    'content': order.details()
                },
                "message": 'Order created',
                "status_code": 200
            }

        except BaseException as e:
            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }

        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def get_active_orders(request):
        """
        Get Restaurants Active orders 
        -----
            {
                restaurant_id:int,
                status: choice(succeeded|pending|served)
                page:1,
                items:10
            }
        """
        data = {}
        try:
            restaurant = Restaurant.objects.get(
                id=request.data['restaurant_id'])
            orders = Order.objects.filter(restaurant=restaurant).filter(
                status__icontains=request.data['status']).order_by('-expected_time')
            page = request.GET.get('page', request.data['page'])
            paginator = Paginator(orders, request.data['items'])
            details = []

            for order in paginator.page(page):
                details.append({
                    'id':order.id,
                    'person': order.person(),
                    'status': order.status,
                    'expected_time': order.expected_time,
                    'order_type': order.order_type,
                    'details': order.details(),
                    'food_instructions': order.food_instructions,
                    'created_at': order.created_at,
                    'updated_at': order.updated_at
                })

            data = {
                "data": details,
                "message": 'deleted',
                "status_code": 200
            }
        except BaseException as e:
            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }

        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def get_total_active_orders(request):
        """
        Get Restaurants Active orders 
        -----
            {
                restaurant_id:int,
                status: choice(succeeded|pending|served)
            }
        """
        data = {}
        try:
            restaurant = Restaurant.objects.get(
                id=request.data['restaurant_id'])
            orders = Order.objects.filter(restaurant=restaurant).filter(
                status=request.data['status'])
            data = {
                "data": {
                    "orders": orders.count(),
                },
                "message": 'deleted',
                "status_code": 200
            }
        except BaseException as e:
            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }

        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def get_booking_time_difference(request):
        """
        Get Persons Orders
        --------
            {
                person_id:int,
            }
        """
        try:
            od = Order.objects.filter(
                person_id=request.data['person_id']).order_by('-id')[0]
            expected = od.expected_time
            current_time = datetime.datetime.now(
                tz=pytz.timezone('Africa/Nairobi'))
            return Response(data={
                "data": {
                    "time_difference": expected - current_time,
                },
                "message": 'OK',
                "status_code": 200
            })

        except BaseException as e:

            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }

            return Response(data)
