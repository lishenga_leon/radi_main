from django.db import models
from food.models import Foodie
from person.models import person
from restaurant.models import Restaurant
from food.serializers import FoodieSerializer

# Create your models here.


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    person_id = models.IntegerField(default=None)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    status = models.CharField(max_length=255, default='PENDING')
    expected_time = models.DateTimeField()
    order_type = models.CharField(max_length=255, default='EATIN')
    food_instructions = models.CharField(max_length=1000, default='')
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

    def details(self):
        details = OrderDetails.objects.filter(order=self)
        data = []
        for detail in details:
            data.append({
                'food': FoodieSerializer(detail.foodie).data,
                'quantity': detail.quantity
            })
            pass
        return data
    
    def person(self):
        user = person.objects.get(id=self.person_id)
        return {
            'id': user.id,
            'fullname': user.fullname,
            'email': user.email,
            # 'password': user.password,
            'msisdn': user.msisdn,
            'status': user.status,
            'stripe_id': user.stripe_id,
            'card_brand ': user.card_brand,
            'profile_pic': '/media/profilepcis/'+user.profile_pic,
            'card_last_four': user.card_last_four,
            'trial_end_at': user.trial_end_at,
            'created_at': user.created_at,
            'updated_at': user.updated_at
        }

    def Restaurantdetails(self):
        det = {
            'id': self.restaurant.id,
            'name': self.restaurant.name,
            'description': self.restaurant.description,
            'location': self.restaurant.location,
            'lat': self.restaurant.lat,
            'long': self.restaurant.long,
            'cover_image': 'media/restaurant/picture/'+self.restaurant.cover_image,
            'status': self.restaurant.status,
            'phone_number': self.restaurant.phone_number,
            'max_capacity': self.restaurant.max_capacity,
            'created_at': self.restaurant.created_at,
            'updated_at': self.restaurant.updated_at
        }
        return det

    # def payments(self):
    #     return payment.objects.filter(order = self)



class Bill(models.Model):
    id = models.AutoField(primary_key=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    person = models.ForeignKey(person, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    status = models.CharField(max_length=255, default='PENDING_PAYMENT')
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)


class OrderDetails(models.Model):
    id = models.AutoField(primary_key=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    foodie = models.ForeignKey(Foodie, on_delete=models.CASCADE,
                               blank=True,
                               null=True)
    quantity = models.IntegerField(default=1)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

    def Fooddetails(self):
        det = {
            'id': self.foodie.id,
            'name': self.foodie.name,
            'description': self.foodie.description,
            'menu_id': self.foodie.menu_id,
            'restaurant_id': self.foodie.restaurant_id,
            'cover_image': 'media/food/picture/'+self.foodie.cover_image,
            'created_at': self.foodie.created_at,
            'updated_at': self.foodie.updated_at
        }
        return det