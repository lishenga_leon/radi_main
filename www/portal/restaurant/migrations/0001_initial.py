# Generated by Django 2.2.1 on 2019-05-20 05:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('person', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RestaturantGallery',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('restaturant_id', models.IntegerField()),
                ('image', models.CharField(default=None, max_length=255)),
                ('image_thumb', models.CharField(default=None, max_length=255)),
                ('created_at', models.DateTimeField(default=None, null=True)),
                ('updated_at', models.DateTimeField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(default=None, max_length=255, null=True)),
                ('description', models.CharField(default='lorem Ipsum', max_length=255)),
                ('location', models.CharField(default=None, max_length=255, null=True)),
                ('lat', models.CharField(default=None, max_length=255, null=True)),
                ('rating', models.IntegerField(default=0)),
                ('long', models.CharField(default=None, max_length=255, null=True)),
                ('status', models.CharField(default=None, max_length=255, null=True)),
                ('max_capacity', models.IntegerField(default=0)),
                ('cover_image', models.CharField(default=None, max_length=255, null=True)),
                ('created_at', models.DateTimeField(default=None, null=True)),
                ('updated_at', models.DateTimeField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='RestaurantTables',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('restaturant_id', models.IntegerField()),
                ('max_capacity', models.IntegerField(default=0)),
                ('current_capacity', models.IntegerField(default=0)),
                ('status', models.CharField(default='EMPTY', max_length=100)),
                ('table_no', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=None, null=True)),
                ('updated_at', models.DateTimeField(default=None, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TableBookings',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('status', models.CharField(default='ACTIVE', max_length=255)),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.person')),
                ('table', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restaurant.RestaurantTables')),
            ],
        ),
    ]
