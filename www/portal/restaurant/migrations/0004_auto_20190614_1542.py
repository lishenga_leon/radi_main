# Generated by Django 2.2 on 2019-06-14 12:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restaurant', '0003_auto_20190614_1459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant',
            name='lat',
            field=models.FloatField(max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='long',
            field=models.FloatField(max_length=10, null=True),
        ),
    ]
