from rest_framework.serializers import ModelSerializer, Serializer, SerializerMethodField
from .models import Restaurant



class RestaurantSerializers(ModelSerializer):
    class Meta:
        model = Restaurant
        fields = '__all__'
