from django.urls import include, path
from . import controller

urlpatterns = [
     path('restaurant/create/', controller.Restaurants.create_restaurant),
     path('restaurant/update/', controller.Restaurants.update_restaurant),
     path('restaurant/delete/', controller.Restaurants.delete_restaurant),
     path('restaurant/getAll/', controller.Restaurants.get_restaurants),
     path('restaurant/findById/', controller.Restaurants.get_restaurant_by_id),
     path('restaurant/Menus/', controller.Restaurants.get_restaurant_menus_by_id),
     path('restaurant/searchByName', controller.Restaurants.search_restaurant),
     path('restaurant/searchProximity',
          controller.Restaurants.get_closest_restaurants),
     path('restaurant/rateRestaurant',
         controller.Restaurants.rate_restaturant),
     path('restaurant/top3ratedRestaurants',
         controller.Restaurants.three_top_restaurants),
     
     # Table Management
     path('restaurant/tables/createTable', controller.Tables.create_table),
     path('restaurant/tables/deleteTable', controller.Tables.delete_table),
     path('restaurant/tables/updateTable', controller.Tables.update_table),
     path('restaurant/tables/findTableById',
          controller.Tables.particular_table),
     path('restaurant/tables/findTablesinRestaurant',
          controller.Tables.restaurant_tables),
     path('restaurant/tables/bookTable', controller.Tables.book_table),
     path('restaurant/tables/unbookTable', controller.Tables.unbook_table),


     # Bill routes
     path('', include('bill.urls')),
]
