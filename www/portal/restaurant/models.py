from django.db import models
from food.models import Menu
from person.models import posts, person, likes, postGallery

# Create your models here.
class Restaurant(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, default=None, null=True)
    description = models.CharField(max_length=255, default='lorem Ipsum')
    location = models.CharField(max_length=255, default=None, null=True)
    lat = models.FloatField(max_length=10,  null=True)
    rating = models.IntegerField(default=0)
    long = models.FloatField(max_length=10, null=True)
    status = models.CharField(max_length=255, default=None, null=True)
    category = models.CharField(max_length=255, default=None, null=True)
    phone_number = models.CharField(max_length=255, default=None, null=True)
    max_capacity = models.IntegerField(default=0)
    cover_image = models.CharField(max_length=255, default=None, null=True)
    created_at = models.DateTimeField(default=None, null=True)
    updated_at = models.DateTimeField(default=None, null=True)

    def menus(self):
        menus = Menu.objects.filter(restaurant_id=self.id)
        values = []
        for men in menus:
            details = {
                'id': men.id,
                'name': men.name,
                'restaurant_id': men.restaurant_id,
                'foods': men.foods(),
                'created_at': men.created_at,
                'updated_at': men.updated_at
            }
            values.append(details)

        return values

    def posts(self):
        poster = posts.objects.filter(restaurant_id=self.id)
        values = []
        for post in poster:
            gallery = postGallery.objects.get(post_id=post.id)
            # liker = likes.objects.filter(post_id=post.id)
            details = {
                'id': post.id,
                'pictureGallery': '/media/posts/picture_thumb/'+gallery.imageThumb,
                'person_id': post.person_id,
                'restaurant_id': post.restaurant_id,
                # 'likes': liker.likes,
                # 'like_id': liker.id,
                # 'like_person_id': liker.person_id,
                'likes': post.likes,
                'description': post.description,
                'created_at': post.created_at,
                'updated_at': post.updated_at,
            }
            values.append(details)
        return values

    def tables(self):
        tables = RestaurantTables.objects.filter(restaurant_id=self.id)
        values = []
        for table in tables:
            details = {
                'id': table.id,
                'max_capacity': table.max_capacity,
                'current_capcacity': table.current_capacity,
                'table_no': table.table_no,
                'created_at': table.created_at,
                'updated_at': table.updated_at
            }
            values.append(details)
        return values


class RestaurantTables(models.Model):
    id = models.AutoField(primary_key=True)
    restaturant_id = models.IntegerField()
    max_capacity = models.IntegerField(default=0)
    current_capacity = models.IntegerField(default=0)
    status = models.CharField(max_length=100, default='EMPTY')
    table_no = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=None, null=True)
    updated_at = models.DateTimeField(default=None, null=True)


class RestaturantGallery(models.Model):

    id = models.AutoField(primary_key=True)
    restaturant_id = models.IntegerField()
    image = models.CharField(max_length=255, default=None)
    image_thumb = models.CharField(max_length=255, default=None)
    created_at = models.DateTimeField(default=None, null=True)
    updated_at = models.DateTimeField(default=None, null=True)


class TableBookings(models.Model):

    id = models.AutoField(primary_key=True)
    table = models.ForeignKey(RestaurantTables, on_delete=models.CASCADE)
    person = models.ForeignKey(person, on_delete=models.CASCADE)
    status = models.CharField(max_length=255, default='ACTIVE')
