import requests
from django.core.paginator import Paginator
from rest_framework.decorators import api_view
from rest_framework.response import Response
from authentication.api.decorators import check_token_status
import datetime
import pytz
from restaurant import helpers
from person.models import posts, person
from restaurant.models import RestaturantGallery, Restaurant, RestaurantTables, TableBookings


class Restaurants():
    @api_view(['POST'])
    @check_token_status()
    def create_restaurant(request):
        """
        create restaurant
        -----
            {
                name : string,
                location : string,
                description :  kjfkfkls,
                lat : float,
                long : float,
                picture : fsfsf......(base64)
                picture_encoding : png
                category :  DRINKS | FOOD |
                phone_number :
                max_capacity : btwn 0 and 1
            }

        """

        data = {}
        try:
            decoder = helpers.base64decoding(
                pictures=request.data['picture'],
                file_type='png'
            )
            restaurant = Restaurant(
                name=request.data["name"],
                location=request.data["location"],
                description=request.data["description"],
                lat=request.data["lat"],
                long=request.data["long"],
                cover_image=decoder['newName'],
                status="ACTIVE",
                category=request.data['category'],
                max_capacity=request.data['max_capacity'],
                phone_number=request.data['phone_number'],
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC)
            )

            restaurant.save()
            data = {
                "data": {},
                "message": 'updated',
                "status_code": 200
            }

        except BaseException as e:
            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }

        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def get_restaurants(request):
        """
        restaurants
        -----
            {
                page : int,
                size : int
            }

        """
        data = {}
        try:
            restaurants = Restaurant.objects.all()
            page = request.GET.get('page', request.data['page'])
            paginator = Paginator(restaurants, request.data['size'])
            details = []
            for rest in paginator.page(page):
                _object = Restaurant.objects.get(id=rest.id)
                values = {
                    'id': rest.id,
                    'name': rest.name,
                    'description': rest.description,
                    'location': rest.location,
                    'lat': rest.lat,
                    'long': rest.long,
                    'menus': rest.menus(),
                    'rating': rest.rating,
                    'cover_image': 'media/restaurant/picture/'+rest.cover_image,
                    'status': rest.status,
                    'phone_number': rest.phone_number,
                    'max_capacity': rest.max_capacity,
                    'created_at': rest.created_at,
                    'updated_at': rest.updated_at
                }

                details.append(values)

            data = {
                "data": details,
                "message": 'updated',
                "status_code": 200
            }

        except BaseException as e:

            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }
        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def get_restaurant_by_id(request):
        """
        -----
            {
                restaurant_id : int
            }

        """
        data = {}
        try:
            restaurant_id = request.data['restaurant_id']
            rest = Restaurant.objects.get(id=restaurant_id)
            details = {
                'id': rest.id,
                'name': rest.name,
                'place': rest.location,
                'location': {
                    'latitude': rest.lat,
                    'longitude': rest.long
                },
                'rating': rest.rating,
                'description': rest.description,
                'lat': rest.lat,
                'phone_number': rest.phone_number,
                'long': rest.long,
                'status': rest.status,
                'rating': rest.rating,
                'max_capacity': rest.max_capacity,
                'menus': rest.menus(),
                'cover_image': '/media/restaurant/picture/'+rest.cover_image,
                'created_at': rest.created_at,
                'updated_at': rest.updated_at
            }

            data = {
                "data": details,
                "message": 'updated',
                "status_code": 200
            }
        except BaseException as e:

            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }

        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def rate_restaturant(request):
        """
        Rate Restaurant
        -----
            {
                restaurant_id : int,
                rating: int | (scale of 1 - 5)
            }

        """
        data = {}
        try:
            restaurant_id = request.data['restaurant_id']
            rest = Restaurant.objects.get(id=restaurant_id)
            rest.rating = (rest.rating + request.data['rating'])/2
            rest.save()
            data = {
                "data": {},
                "message": 'updated',
                "status_code": 200
            }
        except BaseException as e:

            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }

        return Response(data)

    @api_view(['DELETE'])
    @check_token_status()
    def delete_restaurant(request):
        """
        -----
            {
                restaurant_id:int
            }

        """
        data = {}
        try:
            restaurant = Restaurant.objects.get(id=request.data['id'])
            restaurant.delete()
            data = {
                "data": {},
                "message": 'deleted',
                "status_code": 200
            }
        except BaseException as e:

            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }
        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def update_restaurant(request):
        """
        update restaurant
        -----
            {
                restaurant_id:int,
                name:string,
                description: jfkdf,
                location:string,
                lat:float,
                long:float
            }

        """
        data = {}
        try:
            restaurant = Restaurant.objects.get(id=request.data['id'])
            restaurant.name = request.data["name"]
            restaurant.location = request.data["location"]
            restaurant.description = request.data["description"]
            restaurant.lat = request.data["lat"]
            restaurant.max_capacity = request.data["max_capacity"]
            restaurant.long = request.data["long"]
            restaurant.save()
            data = {
                "data": {},
                "message": 'updated',
                "status_code": 200
            }
        except BaseException as e:

            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }
        return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def search_restaurant(request):
        """
        Search for restaurants
        -----
            {
                name:string,
                page: int,
                size: int
            }
        """
        try:
            restaurants = Restaurant.objects.filter(
                name__icontains=request.data['name'])
            page = request.GET.get('page', request.data['page'])
            paginator = Paginator(restaurants, request.data['size'])
            details = []

            for rest in paginator.page(page):
                values = {
                    'id': rest.id,
                    'name': rest.name,
                    'description': rest.description,
                    'place': rest.location,
                    'location': {
                        'latitude': rest.lat,
                        'longitude': rest.long
                    },
                    'lat': rest.lat,
                    'long': rest.long,
                    'menus': rest.menus(),
                    'rating': rest.rating,
                    'max_capacity': rest.max_capacity,
                    'phone_number': rest.phone_number,
                    'cover_image': '/media/restaurant/picture/'+rest.cover_image,
                    'status': rest.status,
                    'created_at': rest.created_at,
                    'updated_at': rest.updated_at
                }

                details.append(values)

            data = {
                "data": details,
                "message": 'updated',
                "status_code": 200
            }

            return Response(data)

        except BaseException as e:

            data = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {

                }
            }
            print(data)
            return Response(data)

    @api_view(['POST'])
    @check_token_status()
    def get_restaurant_menus_by_id(request):
        """
        -----
            {
                restaurant_id:int
            }

        """
        data = {}
        try:
            restaurant_id = request.data['restaurant_id']
            rest = Restaurant.objects.get(id=restaurant_id)
            val = []
            details = {
                'id': rest.id,
                'name': rest.name,
                'location': rest.location,
                'description': rest.description,
                'lat': rest.lat,
                'long': rest.long,
                'status': rest.status,
                'phone_number': rest.phone_number,
                'max_capacity': rest.max_capacity,
                'rating': rest.rating,
                'menus': rest.menus(),
                'cover_image': '/media/restaurant/picture/'+rest.cover_image,
                'created_at': rest.created_at,
                'updated_at': rest.updated_at
            }
            val.append(details)
            data = {
                "data": val,
                "message": 'updated',
                "status_code": 200
            }
            return Response(data)
        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)

    @api_view(['POST'])
    @check_token_status()
    def get_closest_restaurants(request):
        """
        Search Restaurants located within the define radius on coverage

        ---

            {
                latitude: 25.1212,
                longitude: 55.1535,
                radius: 5 (In Kms)
            }
        """
        try:

            loc = helpers.GeoLocation.from_degrees(
                request.data['latitude'], request.data['longitude'])
            distance = request.data['radius']  # 1 kilometer

            SW_loc, NE_loc = loc.bounding_locations(distance)

            Hlocation = {
                'SW_loc': {
                    'lat': SW_loc.deg_lat,
                    'long': SW_loc.deg_lon,
                },
                'NE_loc': {
                    'lat': NE_loc.deg_lat,
                    'long': NE_loc.deg_lon,
                }
            }

            restaurants = Restaurant.objects.filter(
                lat__range=(SW_loc.deg_lat, NE_loc.deg_lat)).filter(
                long__range=(SW_loc.deg_lon, NE_loc.deg_lon))
            details = []
            for rest in restaurants:
                values = {
                    'id': rest.id,
                    'name': rest.name,
                    'description': rest.description,
                    'location': {
                        'latitude': rest.lat,
                        'longitude': rest.long,
                    },
                    'status': rest.status,
                    'category': rest.category,
                    'phone_number': rest.phone_number,
                    'max_capacity': rest.max_capacity,
                    'rating': rest.rating,
                    'cover_image': '/media/restaurant/picture/'+rest.cover_image,
                    'created_at': rest.created_at,
                    'updated_at': rest.updated_at
                }

                details.append(values)

            data = {
                "data": details,
                "message": 'loaded',
                "status_code": 200
            }

            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)

    @api_view(['POST'])
    @check_token_status()
    def three_top_restaurants(request):
        """
        3 most rated restaurants

        """
        from random import randint
        try:
            restaurants = Restaurant.objects.order_by('-rating')[:3]
            details = []
            for rest in restaurants:
                values = {
                    'id': rest.id,
                    'name': rest.name,
                    'description': rest.description,
                    'location': {
                        'latitude': rest.lat,
                        'longitude': rest.long,
                    },
                    'status': rest.status,
                    'category': rest.category,
                    'phone_number': rest.phone_number,
                    'max_capacity': rest.max_capacity,
                    'rating': rest.rating,
                    'cover_image': '/media/restaurant/picture/'+rest.cover_image,
                    'created_at': rest.created_at,
                    'updated_at': rest.updated_at,
                    'post': rest.posts()[randint(0, len(rest.posts())-1)]
                }

                details.append(values)

            data = {
                "data": details,
                "message": 'loaded',
                "status_code": 200
            }

            return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)


class Tables():

    @api_view(['POST'])
    @check_token_status()
    def create_table(request):
        """
        Create Table
        -----------
            {
                max_capacity:int
                table_no:int
                restaurant_id:int
            }
        """

        try:
            tables = RestaurantTables(
                max_capacity=request.data['max_capacity'],
                table_no=request.data['table_no'],
                restaurant_id=request.data['restaurant_id']
            )
            tables.save()

            data = {
                "data": {},
                "message": 'created',
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:
            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)

    @api_view(['PUT'])
    @check_token_status()
    def update_table(request):
        """
        Update Table
        -----------
            {

                max_capacity:int
                table_no:int
                table_id:int
            }
        """
        data = {}
        try:
            table = RestaurantTables.objects.get(id=request.data['table_id'])
            table.max_capacity = request.data['max_capacity']
            table.table_no = request.data['table_no']
            table.save()

            data = {
                "data": {},
                "message": 'updated',
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:
            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)

    @api_view(['PUT'])
    @check_token_status()
    def book_table(request):
        """
        Book Table
        -----------
            {
                people:int
                table_id:int
                person_id:int
            }
        """
        data = {}
        try:
            owner = person.objects.get(id=request.data['person_id'])

            table = RestaurantTables.objects.get(id=request.data['table_id'])

            if table.status is 'OCCUPIED':
                error = {
                    'status_code': 500,
                    'message': 'Table is occupied',
                    'data': {}
                }
                return Response(error)
            else:
                table.status = 'OCCUPIED'

            if table.current_capacity >= table.max_capacity:
                error = {
                    'status_code': 500,
                    'message': 'Table max capacity attained',
                    'data': {}
                }
                return Response(error)
            else:
                table.current_capacity = table.current_capacity + \
                    request.data['people']

            table.save()

            bookings = TableBookings(
                table=table,
                person=owner
            )
            bookings.save()

            data = {
                "data": {},
                "message": 'updated',
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:
            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)

    @api_view(['POST'])
    @check_token_status()
    def unbook_table(request):
        """
        Unbook Table
        -----------
            {
                table_id:int
                person_id:int
            }
        """
        try:
            table = RestaurantTables.objects.get(id=request.data['table_id'])
            table.status = 'EMPTY'
            table.current_capacity = 0
            table.save()
            owner = person.objects.get(id=request.data['person_id'])
            booking = TableBookings.objects.get(
                person=owner, table=table, status='ACTIVE')
            booking.status = 'DISABLED'
            booking.save()
            data = {
                "data": {},
                "message": 'updated',
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:
            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)

    @api_view(['GET'])
    @check_token_status()
    def particular_table(request):
        """
        Particular Table Details
        -----------
            {
                table_id:int
            }
        """
        data = {}
        try:
            table = RestaurantTables.objects.get(id=request.data['table_id'])
            details = {
                'id': table.id,
                'max_capacity': table.max_capacity,
                'current_capacity': table. current_capacity,
                'table_no': table.table_no,
                'created_at': table.created_at,
                'updated_at': table.updated_at,
            }
            data = {
                "data": details,
                "message": 'updated',
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:
            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)

    @api_view(['DELETE'])
    @check_token_status()
    def delete_table(request):
        """
        Delete Table
        -----------
            {
                table_id:int
            }
        """
        data = {}
        try:
            table = RestaurantTables.objects.get(id=request.data['table_id'])
            table.delete()
            data = {
                "data": {},
                "message": 'updated',
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:
            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)

    @api_view(['GET'])
    @check_token_status()
    def restaurant_tables(request):
        """
        Restaurant Tables
        -----------
            {
                restaurant_id:int
            }
        """
        data = {}
        try:
            restaurant_id = request.data['restaurant_id']
            rest = Restaurant.objects.get(id=restaurant_id)
            data = {
                "data": rest.tables(),
                "message": 'updated',
                "status_code": 200
            }
            return Response(data)

        except BaseException as e:
            error = {
                'status_code': 500,
                'message': 'error'+str(e),
                'data': {}
            }
            return Response(error)
