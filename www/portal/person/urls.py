# from django.contrib.auth.models import User
from django.urls import include, path

from person.logic import person, posts

urlpatterns = [
    
    #Person routes
    path('person/createperson/', person.create_person),
    path('person/updateperson/', person.update_person),
    path('person/person_device_uid/', person.person_device_uid),
    path('person/deleteperson/', person.delete_person),
    path('person/getallpersons/', person.get_all_persons),
    path('person/email_login/', person.get_person_email_login),
    path('person/resetpersonpassword/',person.update_person_password),
    path('person/getparticularperson/', person.get_particular_person_details),
    path('person/uploadprofilepic/', person.upload_profile_pic),
    path('person/googleFacebookSignIn/', person.googleFacebookSignIn),
    path('person/sendPushNotification/', person.initiatePushNotification),
    path('person/sendEmailForgotPassword', person.sendMailPasswordForgotten),
    path('person/registerDeviceIdForLoggin', person.registerPhonesLoggedInWithoutUser),
    path('person/encodeImageBase64', person.encodeImageBase64),

    #transactions routes
    path('', include('payments.urls')),


    #Post routes
    path('post/createpost/', posts.create_post),
    path('post/getParticularPersonPosts/', posts.get_particular_person_posts),
    path('post/deletePost/', posts.delete_post),
    path('post/updatePost/', posts.update_post),
    path('post/getPostsDisplayOnApp/', posts.get_posts_display_on_app),
    path('post/like/', posts.add_likes),
    path('post/unlike/', posts.delete_like),
    path('post/displayPostsSorted',posts.sort_posts_likes_based),

]
