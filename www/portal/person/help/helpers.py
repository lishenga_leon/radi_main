from person.models import logs, person
from person.serializer import PersonSerializer
from authentication.api import views
import datetime, pytz, base64, os
from passlib.hash import django_pbkdf2_sha256 as password_handler
from pyfcm import FCMNotification
from django.conf import settings
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# function for a creating a log when a user has been created
def create_log(request, tags, description):
    log = logs(
        name= "logs",
        tags="person", 
        description="Person Created", 
        created_at = datetime.datetime.now(tz=pytz.UTC), 
        updated_at= datetime.datetime.now(tz=pytz.UTC)
    )
    log.save()
    return True

# function for checking if a user ever logged in with google or not
def userlogin(email):
    persons = person.objects.get(email=email)
    token = views.get_token_person_id(person_id=persons.id)
    success = {
        'message': 'success',
        'data': PersonSerializer(persons).data,
        'status_code': 200,
        'tok': {
            'token': token
        }
    }

    return success

# function for encoding an image
def encodeImage(image):
    encoded_string=''
    with open(find(image, path), "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    success = {
        'message': 'success',
        'data': encoded_string,
        'status_code': 200
    }

    return success

def find(image, path):
    for root, dirs, files in os.walk(path):
        if image in files:
            return os.path.join(root, image)

# function for initiating a push notification
def pushNotification(device_uid, body):
    push_service = FCMNotification(api_key=settings.FIREBASE_LEGACY_KEY)
    # Send to multiple devices by passing a list of ids.
    registration_ids = device_uid
    message_title = "Radi"
    message_body = body
    result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title, message_body=message_body)
    print(result)
    success={
        'message': 'Push notification success',
        'data': result,
        'status_code': 200
    }
    return success

# function for sending email using sendgrid
def sendMail(sender, receiver, subject, content):
    try:
        message = Mail(
            from_email=sender,
            to_emails=receiver,
            subject=subject,
            html_content=content
        )
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        response = sg.send(message)
        success={
            'message': 'Email sent',
            'data': response,
            'status_code': 200
        }
        return success
    except Exception as e:
        error={
            'message': 'error:' + str(e),
            'status_code': 500
        }
        return error

