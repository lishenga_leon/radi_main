from rest_framework.serializers import ModelSerializer, Serializer, SerializerMethodField
from .models import person
from restaurant.serializers import RestaurantSerializers



class PersonSerializer(ModelSerializer):
    # restaurant = RestaurantSerializers()
    class Meta:
        model = person
        fields = '__all__'
