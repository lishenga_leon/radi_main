from django.core.paginator import Paginator
from django.db import models
from django.utils import timezone
import datetime
import pytz
# from restaurant.models import Restaurant


# Table for all the persons in the Radi application
class person(models.Model): 

    id = models.AutoField(primary_key=True)
    fullname = models.CharField(
        max_length=255, default=None, blank=True, null=True)
    email = models.CharField(max_length=255, default=None, unique=True)
    password = models.CharField(max_length=255, default=None)
    msisdn = models.CharField(max_length=255, default=None)
    status = models.IntegerField(default=0)
    profile_pic = models.CharField(max_length=255, default=None)
    device_uid = models.CharField(max_length=255, default='JESUS')
    stripe_id = models.CharField(max_length=255, default=None)
    card_brand = models.CharField(max_length=255, default=None)
    card_last_four = models.CharField(max_length=255, default=None)
    date_card_added = models.DateTimeField(default=timezone.now())
    trial_end_at = models.CharField(max_length=255, default=None)
    restaurant_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
            self.date_card_added = timezone.now()

        self.updated_at = timezone.now()
        return super(person, self).save(*args, **kwargs)


# log table when a person is logged into the system
class logs(models.Model):

    id = models.AutoField(primary_key=True)
    description = models.CharField(
        max_length=255, default=None, blank=True, null=True)
    tags = models.CharField(
        max_length=255, default=None, blank=True, null=True)
    name = models.CharField(
        max_length=255, default=None, blank=True, null=True)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)


# table for all the posts a person makes about a resturant
class posts(models.Model):
    id = models.AutoField(primary_key=True)
    person_id = models.IntegerField(default=None)
    restaurant_id = models.IntegerField(default=None)
    likes = models.IntegerField(default=0)
    description = models.CharField(
        max_length=1000, default=None, blank=True, null=True)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

    def poster(self):
        user = person.objects.get(id=self.person_id)
        return {
            'id': user.id,
            'fullname': user.fullname,
            'email': user.email,
            'password': user.password,
            'msisdn': user.msisdn,
            'status': user.status,
            'stripe_id': user.stripe_id,
            'card_brand ': user.card_brand,
            'profile_pic': '/media/profilepcis/'+user.profile_pic,
            'card_last_four': user.card_last_four,
            'trial_end_at': user.trial_end_at,
            'created_at': user.created_at,
            'updated_at': user.updated_at
        }

    def images(self, request):
        gallery = postGallery.objects.filter(post_id=self.id)
        page = request.GET.get('page', 1)
        paginator = Paginator(gallery, 10)
        details = []

        for image in paginator.page(page):
            values = {
                'imageThumb': '/media/posts/picture_thumb/'+image.imageThumb
            }
            details.append(values)

        return details


# table for all the photos a person uploads for a specific resturant
class postGallery(models.Model):

    id = models.AutoField(primary_key=True)
    post_id = models.IntegerField(default=None)
    image = models.CharField(max_length=255, default=None)
    imageThumb = models.CharField(max_length=255, default=None)
    base64Pic = models.CharField(max_length=1000000, default='')
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)


# table for likes of posts


class likes(models.Model):
    id = models.AutoField(primary_key=True)
    post_id = models.IntegerField(default=None)
    person_id = models.IntegerField(default=None)
    likes = models.IntegerField(default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)


# Table linking the like table and the people who liked a post
class liked_persons(models.Model):
    id = models.AutoField(primary_key=True)
    like_id = models.IntegerField(default=None)
    person_id = models.IntegerField(default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)
