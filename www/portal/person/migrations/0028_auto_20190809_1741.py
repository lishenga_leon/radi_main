# Generated by Django 2.2 on 2019-08-09 14:41

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0027_auto_20190809_1232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='date_card_added',
            field=models.DateTimeField(default=datetime.datetime(2019, 8, 9, 14, 41, 18, 61292, tzinfo=utc)),
        ),
    ]
