# Generated by Django 2.2 on 2019-06-28 02:07

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0010_auto_20190626_0603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='date_card_added',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 28, 2, 7, 13, 973209, tzinfo=utc)),
        ),
    ]
