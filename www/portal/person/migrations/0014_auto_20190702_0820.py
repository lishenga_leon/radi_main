# Generated by Django 2.2.1 on 2019-07-02 08:20

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0013_auto_20190628_0213'),
    ]

    operations = [
        migrations.AddField(
            model_name='postgallery',
            name='base64Pic',
            field=models.CharField(default='', max_length=10000),
        ),
        migrations.AlterField(
            model_name='person',
            name='date_card_added',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 2, 8, 20, 49, 179541, tzinfo=utc)),
        ),
    ]
