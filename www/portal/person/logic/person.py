import datetime
import json
import os
import random
import string

import pytz
import requests
from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.core.paginator import Paginator
from passlib.hash import django_pbkdf2_sha256 as password_handler
from PIL import Image
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from authentication.api.decorators import check_token_status
from authentication.api import views

from analytics.models import visits
from payments.help import helpers as helping
from person.help import helpers
from person.models import person
from person.serializer import PersonSerializer

# from restaurant.models import Restaurant

PAYMENTSERVICE = settings.PAYMENTSERVICE
HEADERS = settings.HEADERS
IMAGE_PATH = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))


# function for creating a Radi User
@api_view(['POST'])
def create_person(request):
    """
    Create Person
    -----
        {

            fullname:leon lishenga,
            email:leon@yahoo.com,
            password:roshie,
            msisdn:083726493,
            location:sample
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':
            persons = person(
                fullname=request.data['fullname'],
                email=request.data['email'],
                password=make_password(request.data['password']),
                msisdn=request.data['msisdn'],
                status='1',
                stripe_id='0',
                card_brand='0',
                card_last_four='0',
                profile_pic='null',
                trial_end_at='0',
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC)
            )
            persons.save()

            payload = {
                "email": request.data['email']
            }
            try:
                r = requests.post(
                    PAYMENTSERVICE+'/registration/createstripeperson/', data=payload, headers=HEADERS)
                data = r.json()
            except BaseException as e:
                data = {
                    "data": {
                        "id": "nfsjdnfaojd"
                    }
                }
            people = person.objects.get(email=request.data['email'])
            people.stripe_id = data['data']['id']
            people.save()

            views.create_token_person_id(person_id=people.id)

            log = helpers.create_log(request=request, tags="Persons", description="Person Created")

            content="<strong>Thank you for joining us </strong>"
            subject='User created'
            sender='lishengaleon@gmail.com'

            send = helpers.sendMail(sender=sender, receiver=request.data['email'], content=content, subject=subject)

            success = {
                'message': 'success',
                'log': log,
                'status_code': 200
            }
            return Response(success)

    except BaseException as e:
        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# function for logging in phones that have skipped the login or create account process
@api_view(['POST'])
def registerPhonesLoggedInWithoutUser(request):
    """
    Unique device ID
    -----
        {
            deviceId:jdhadasd,
        }
    """
    try:
        token = views.create_token_device_id(device_id=request.data['deviceId'])
        success = {
            'status_code': 200,
            'message': 'Token created',
        }
        tok={
            'token':token.token
        }
        return Response(success, status=None, template_name=None, headers=tok, content_type=None)

    except BaseException as e:
        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# function for checking a user has ever signed up with google or facebook
@api_view(['POST'])
def googleFacebookSignIn(request):
    """
    Google or Facebook check sign in
    -----
        {
            email:leon@yahoo.com,
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':
            if person.objects.filter(email=request.data['email']).exists():
                login = helpers.userlogin(email=request.data['email'])
                return Response(login, status=None, template_name=None, headers=login['tok'], content_type=None)
            else:
                error = {
                    'status_code': 500,
                    'message': 'Person doesnt exist',
                }
                return Response(error)

    except BaseException as e:
        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# function for initiating a push notification
@api_view(['POST'])
@check_token_status()
def initiatePushNotification(request):
    """
    Initiate a push notification
    -----
        {
            person_id: 1, 
            body: jfjd
        }
    """
    try:
        people = person.objects.get(id=request.data['person_id'])
        data=[]
        data.append(people.device_uid)
        push = helpers.pushNotification(device_uid=data, body=request.data['body'])
        return Response(push)

    except BaseException as e:
        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# function for initiating a push notification
@api_view(['POST'])
@check_token_status()
def encodeImageBase64(request):
    """
    Encode image base64
    -----
        {
            image: hdhkad.png, 
        }
    """
    try:
        encode = helpers.encodeImage(image=request.data['image'])
        return Response(encode)

    except BaseException as e:
        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# function for sending an email when a user has forgotten password
@api_view(['POST'])
@check_token_status()
def sendMailPasswordForgotten(request):
    """
    Send email for password forgotten
    -----
        {
            email: lishengaleon@gmail.com
        }
    """
    try:
        sender='lishenga.leon@students.jkuat.ac.ke'
        receiver=request.data['email']

        size=10 
        chars=string.ascii_uppercase + string.digits
        new_password = ''.join(random.choice(chars) for _ in range(size))

        people = person.objects.get(email=request.data['email'])
        people.password = make_password(new_password)
        people.save()

        content="<strong>Your new password is "+new_password+"</strong>"
        subject='Password change'

        send = helpers.sendMail(sender=sender, receiver=receiver, content=content, subject=subject)
        return Response(send)

    except BaseException as e:
        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# upload profile picture for person
@api_view(['POST'])
@check_token_status()
def upload_profile_pic(request):
    """
    Upload person profile picture
    -----
        {
            person_id:1,
            picture:leon,
            token: kjfsjlf
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST' and request.FILES['picture']:

            persons = person.objects.get(id=request.data['person_id'])

            newName = persons.email+'.png'
            basewidth = 300
            img = Image.open(request.FILES['picture'])
            wpercent = (basewidth/float(img.size[0]))
            hsize = int((float(img.size[1])*float(wpercent)))
            img = img.resize((basewidth, hsize), Image.ANTIALIAS)
            img_thumb = img.save(
                IMAGE_PATH+'/media/profilepics/'+newName, format="png", quality=70)

            persons.profile_pic = newName
            persons.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
            'data': {}
        }
        return Response(error)


# update existing person
@api_view(['POST'])
@check_token_status()
def update_person(request):
    """
    Update Person details
    -----
        {
            id:1,
            fullname:leon lishenga,
            email:leon@yahoo.com,
            msisdn: jtown, 
            token: jkfskdk
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':

            persons = person.objects.get(id=request.data['id'])
            persons.fullname = request.data['fullname']
            persons.email = request.data['email']
            persons.msisdn = request.data['msisdn']
            persons.password = make_password(request.data['password'])
            persons.updated_at = datetime.datetime.now(tz=pytz.UTC)
            persons.save()
            success = {'message': 'success', 'status_code': 200}
            return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
            'data': {}
        }
        return Response(error)


# add android or IOS device UID
@api_view(['POST'])
@check_token_status()
def person_device_uid(request):
    """
    Add persons android or IOS device UID 
    -----
        {
            person_id:1,
            device_uid:aksdhjashja65546,
            token:ljfj
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':
            persons = person.objects.get(id=request.data['person_id'])
            persons.device_uid = request.data['device_uid']
            persons.save()
            success = {'message': 'success', 'status_code': 200}
            return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
            'data': {}
        }
        return Response(error)


# update existing user  password
@api_view(['POST'])
@check_token_status()
def update_person_password(request):
    """ 
    Update User Password
    -----
        {
            id:1,
            password:123456,
            token:kjfdjls
        } 
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':

            persons = person.objects.get(id=request.data['id'])
            persons.password = make_password(request.data['password'])
            persons.save()
            success = {
                'message': 'success',
                'status_code': 200,
                'data': {}
            }
            return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
            'data': {}
        }
        return Response(error)


# get all existing users
@api_view(['POST'])
@check_token_status()
def get_all_persons(request):
    """
    See all users 
    -----
        {
            page:1
            items: 5,
            token: khsjsds
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':

            persons = person.objects.all()
            page = request.GET.get('page', request.data['page'])
            paginator = Paginator(persons, request.data['items'])
            details = []
            for user in paginator.page(page):
                values = {
                    'id': user.id,
                    'fullname': user.fullname,
                    'email': user.email,
                    'password': user.password,
                    'msisdn': user.msisdn,
                    'status': user.status,
                    'stripe_id': user.stripe_id,
                    'card_brand ': user.card_brand,
                    'profile_pic': '/media/profilepcis/'+user.profile_pic,
                    'card_last_four': user.card_last_four,
                    'date_card_added': user.date_card_added,
                    'device_uid': user.device_uid,
                    'trial_end_at': user.trial_end_at,
                    'created_at': user.created_at,
                    'updated_at': user.updated_at
                }

                details.append(values)

            data = {
                'data': details,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
            'data': {}
        }
        return Response(error)


# get one particelar users details
@api_view(['POST'])
@check_token_status()
def get_particular_person_details(request):
    """
    Get particular user details
    -----
        {
            person_id:1,
            token:hksfhks
        }
    """
    try:
        if request.method == 'GET':
            success = {'message': 'method not allowed', 'status_code': 401}
            return Response(success)

        elif request.method == 'POST':

            person_id = request.data['person_id']
            persons = person.objects.get(id=person_id)
            details = {
                'id': persons.id,
                'fullname': persons.fullname,
                'email': persons.email,
                'msisdn': persons.msisdn,
                'password': persons.password,
                'status': persons.status,
                'stripe_id': persons.stripe_id,
                'restaurant_id': persons.restaurant_id,
                'card_brand ': persons.card_brand,
                'profile_pic': '/media/profilepcis/'+user.profile_pic,
                'card_last_four': persons.card_last_four,
                'date_card_added': persons.date_card_added,
                'trial_end_at': persons.trial_end_at,
                'created_at': persons.created_at,
                'updated_at': persons.updated_at
            }

            data = {'data': details, 'message': 'success', 'status_code': 200}

            return Response(data)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
            'data': {}
        }
        return Response(error)


# delete a specific person for radi application
@api_view(['DELETE'])
@check_token_status()
def delete_person(request):
    """
    remove person
    -----
        {
            id:1,
            token:hkdjlew
        }

    """
    try:
        if request.method == 'DELETE':

            _id = request.data['id']
            stripe.api_key = STRIPE_SECRET_KEY
            get_user = person.objects.get(id=_id)

            if get_user.stripe_id == "0":

                delete = person.objects.get(id=_id)
                os.remove(IMAGE_PATH+'/media/profilepics/' +
                          delete.profile_pic, dir_fd=None)
                delete.delete()
                data = {
                    "data": "Person deleted",
                    "message": delete,
                    "status_code": 200
                }
                return Response(data)

            else:
                cu = stripe.Customer.retrieve(get_user.stripe_id)
                delete_stripe = cu.delete()
                delete = person.objects.get(id=_id)
                os.remove(IMAGE_PATH+'/media/profilepics/' +
                          delete.profile_pic, dir_fd=None)
                delete.delete()
                data = {
                    "data": delete_stripe,
                    "message": 'Person deleted',
                    "status_code": 200
                }
                return Response(data)

        else:
            snippets = {

                'message': "invalid request",
                "status_code": 401
            }
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error'+str(e),
            'data': {

            }
        }
        return Response(error)


# Login for a particular person
@api_view(['POST'])
def get_person_email_login(request):
    """
    Login for a particular person
    -----
        {
            email:roshie@gmail.com,
            password:roshie,
        }
    """
    try:
        user_id = request.data['email']
        user_input_pass = request.data['password']
        persons = person.objects.get(email=user_id)
        token = views.get_token_person_id(person_id=persons.id)
        if user_input_pass is None:
            if password_handler.verify('', persons.password):
                success = {
                    'data': {
                        'id': persons.id,
                        'fullname': persons.fullname,
                        'email': persons.email,
                        'password': persons.password,
                        'status': persons.status,
                        'msisdn': persons.msisdn,
                        'stripe_id': persons.stripe_id,
                        'restaurant_id': persons.restaurant_id,
                        'card_brand': persons.card_brand,
                        'date_card_added': persons.date_card_added,
                        'profile_pic': '/media/profilepcis/' + persons.profile_pic,
                        'card_last_four': persons.card_last_four,
                        'trial_end_at': persons.trial_end_at,
                        'created_at': persons.created_at,
                        'updated_at': persons.updated_at,
                    },

                    'status_code': 200,
                }

                tok = {
                    'token': token
                }

                return Response(success, status=None, template_name=None, headers=tok, content_type=None)

            else:
                success = {
                    'message': 'Error',
                    'status_code': 500
                }

                return Response(success)

        else:
            if password_handler.verify(user_input_pass, persons.password):
                success = {
                    'data': {
                        'id': persons.id,
                        'fullname': persons.fullname,
                        'email': persons.email,
                        'password': persons.password,
                        'status': persons.status,
                        'msisdn': persons.msisdn,
                        'stripe_id': persons.stripe_id,
                        'restaurant_id': persons.restaurant_id,
                        'card_brand': persons.card_brand,
                        'date_card_added': persons.date_card_added,
                        'profile_pic': '/media/profilepcis/' + persons.profile_pic,
                        'card_last_four': persons.card_last_four,
                        'trial_end_at': persons.trial_end_at,
                        'created_at': persons.created_at,
                        'updated_at': persons.updated_at,
                        'token': token
                    },
                    'status_code': 200,
                }

                tok = {
                    'token': token
                }

                return Response(success, status=None, template_name=None, headers=tok, content_type=None)

            else:
                success = {
                    'message': 'Error',
                    'status_code': 500
                }

                return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
        return Response(error)
