import datetime
import json
import os
import random
import string

import pytz
import requests
from django.contrib.auth.hashers import make_password
from django.core.paginator import Paginator
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

import stripe
from authentication.api.decorators import check_token_status
from passlib.hash import django_pbkdf2_sha256 as password_handler
from payments.help import helpers as helping
from person.help import decoding, helpers
from person.models import liked_persons, likes, person, postGallery, posts
from PIL import Image
from restaurant.models import Restaurant
from advert import helpers as ad_helper

IMAGE_PATH = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))


@api_view(['POST'])
@check_token_status()
def create_post(request):
    """
    Create Post for person
    -----
        {

            person_id:1
            restaurant_id:jdkja
            picture: yemajndkjsdk........(base64 encoded)
            description:jsjkhdljs
            token:hjdhksd
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':

            post = posts(
                person_id=request.data['person_id'],
                restaurant_id=request.data['restaurant_id'],
                description=request.data['description'],
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC)
            )
            post.save()
            posting = posts.objects.filter(description=request.data['description']).filter(
                restaurant_id=request.data['restaurant_id']).get(person_id=request.data['person_id'])

            decoder = decoding.base64decoding(
                pictures=request.data['picture'],
                file_type='png'
            )
            gallery = postGallery(
                post_id=posting.id,
                image=decoder['newName'],
                imageThumb=decoder['newName_thumb'],
                base64Pic=request.data['picture'],
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC)
            )
            gallery.save()

            like = likes(
                post_id=posting.id,
                likes=0,
                person_id=0,
                created_at=datetime.datetime.now(tz=pytz.UTC),
                updated_at=datetime.datetime.now(tz=pytz.UTC),
            )
            like.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# Get all the all the posts for a person
@api_view(['POST'])
@check_token_status()
def get_particular_person_posts(request):
    """
    Get all the all the posts for a person
    -----
        {
            person_id:1,
            page : 1
            items: 10
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST' and request.FILES['picture']:

            post = posts.objects.filter(
                person_id=request.data['person_id'])
            deta = []
            for poster in post:
                val = {
                    'id': poster.id,
                    'restaurant_id': poster.restaurant_id,
                    'description': poster.description,
                    'created_at': poster.created_at,
                    'updated_at': poster.updated_at
                }

                deta.append(val)

            page = request.GET.get('page', request.data['page'])
            paginator = Paginator(deta, request.data['items'])
            details = []

            for cats in paginator.page(page):
                gallery = postGallery.objects.get(post_id=cats['id'])

                values = {
                    'id': cats['id'],
                    'title': cats['title'],
                    'description': cats['description'],
                    'base64Pic': cats['base64Pic'],
                    'image': '/media/posts/picture_thumb/'+gallery.image,
                    'created_at': cats['created_at'],
                    'updated_at': cats['updated_at']
                }

                details.append(values)

            data = {
                'data': details,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# delete a specific post for person
@api_view(['DELETE'])
@check_token_status()
def delete_post(request):
    """
    Delete post for person
    -----
        {
            post_id:1,
            token:hkdjlew
        }

    """
    try:
        if request.method == 'DELETE':

            _id = request.data['post_id']

            delete_post = posts.objects.filter(id=_id).delete()
            delete_gallery = postGallery.objects.filter(post_id=_id)
            for deleting in delete_gallery:
                os.remove(IMAGE_PATH+'/media/posts/picture_thumb' +
                          deleting.image, dir_fd=None)
                deleting.delete()

            data = {
                "message": "Post deleted",
                "status_code": 200
            }
            return Response(data)

        else:
            snippets = {

                'message': "invalid request",
                "status_code": 401
            }
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
        return Response(error)


# update existing post
@api_view(['POST'])
@check_token_status()
def update_post(request):
    """
    Update Post details
    -----
        {
            post_id:1
            restaurant_id:kjdajdla,
            picture: leon.jpg
            description:jsjkhdljs
            token:hjdhksd
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':

            post = posts.objects.get(id=request.data['post_id'])
            post.restaurant_id = request.data['restaurant_id']
            post.description = request.data['description']
            post.updated_at = datetime.datetime.now(tz=pytz.UTC)
            post.save()

            gallery = postGallery.objects.get(
                post_id=request.data['post_id'])
            decoder = decoding.base64decoding(
                pictures=request.data['picture'],
                file_type='png'
            )

            gallery.image = decoder['newName']
            gallery.imageThumb = decoder['newName_thumb']
            gallery.base64Pic = request.data['picture']
            gallery.updated_at = datetime.datetime.now(tz=pytz.UTC)
            gallery.save()

            success = {
                'message': 'success',
                'status_code': 200
            }
            return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# Get all the all the posts basing on the location and time posted
@api_view(['POST'])
@check_token_status()
def get_posts_display_on_app(request):
    """
    Get all the all the posts basing on the location and time posted
    -----
        {
            page : 1
            items: 10
            person_id: 1
        }
    """
    try:
        if request.method == 'GET':
            snippets = 'success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':

            persons = person.objects.all()
            deta = []
            post = posts.objects.all().order_by('-updated_at')
            for peeps in persons:
                for idx, poster in post:
                    '''
                    Checking so as to produce ad after N no of posts 
                    '''
                    ad = ad_helper.filter_ads()
                    if idx % 5 == 0 and idx != 0 and ad:
                        val = {
                            'id': ad.id,
                            'poster_name': "",
                            'restaurant_id': "",
                            'description': "",
                            'type': 'AD',
                            'created_at': ad.created_at,
                            'updated_at': ad.updated_at
                        }
                        deta.append(val)

                    if peeps.id == poster.person_id:
                        val = {
                            'id': poster.id,
                            'poster_name': peeps.fullname,
                            'restaurant_id': poster.restaurant_id,
                            'description': poster.description,
                            'created_at': poster.created_at,
                            'updated_at': poster.updated_at
                        }
                        deta.append(val)

            page = request.GET.get('page', request.data['page'])
            paginator = Paginator(deta, request.data['items'])
            details = []

            for cats in paginator.page(page):
                gallery = postGallery.objects.get(post_id=cats['id'])
                like = likes.objects.get(post_id=cats['id'])
                values = {
                    'id': cats['id'],
                    'restaurant_id': cats['restaurant_id'],
                    'poster_name': cats['poster_name'],
                    'description': cats['description'],
                    'total': like.likes,
                    'like_id': like.id,
                    'person_id': like.person_id,
                    'imageThumb': '/media/posts/picture_thumb/'+gallery.imageThumb,
                    'base64Pic': gallery.base64Pic,
                    'created_at': cats['created_at'],
                    'updated_at': cats['updated_at']
                }
                details.append(values)

            da = []
            liker = liked_persons.objects.filter(
                person_id=request.data['person_id'])
            for liking in liker:
                va = {
                    'like_id': liking.like_id,
                    'person_id': liking.person_id,
                }
                da.append(va)

            data = {
                'data': details,
                'message': 'success',
                'status_code': 200
            }
            return Response(data)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        print(error)
        return Response(error)


# Create a like for a specific post
@api_view(['POST'])
@check_token_status()
def add_likes(request):
    """
    Create a like
    -----
        {
            token: kfjfs
            post_id:1,
            person_id: 1
        }
    """
    try:
        like = likes(
            post_id=request.data['post_id'],
            likes=1,
            person_id=request.data['person_id'],
            created_at=datetime.datetime.now(tz=pytz.UTC),
            updated_at=datetime.datetime.now(tz=pytz.UTC),
        )
        like.save()

        post = posts.objects.get(id=request.data['post_id'])
        post.likes = post.likes + 1
        post.save()

        success = {
            'message': 'success',
            'total': post.likes,
            'status_code': 200
        }
        return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


# Delete a like for a specific post
@api_view(['POST'])
@check_token_status()
def delete_like(request):
    """
    Delete a like
    -----
        {
            post_id:1,
            person_id: 1
        }
    """
    try:
        like = likes.objects.filter(post_id=request.data['post_id']).get(
            person_id=request.data['person_id'])
        like.delete()
        post = posts.objects.get(id=request.data['post_id'])
        post.likes = post.likes - 1
        post.save()
        success = {
            'message': 'success',
            'total': post.likes,
            'status_code': 200
        }
        return Response(success)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)


@api_view(['POST'])
@check_token_status()
def sort_posts_likes_based(request):
    """
        {
            page : 1
            items: 10
        }
    """
    try:
        restaurants = Restaurant.objects.order_by('rating').reverse()
        page = request.data['page']
        paginator = Paginator(restaurants, request.data['items'])
        values = []
        for rest in paginator.page(page):
            poster = posts.objects.filter(restaurant_id=rest.id)
            for idx, post in enumerate(poster):
                gallery = postGallery.objects.get(post_id=post.id)
                liker = {}
                try:
                    liker = likes.objects.filter(
                        post_id=post.id).latest('created_at')
                except BaseException as e:
                    liker = {
                        'likes': None,
                        'like_id': None,
                        'like_person_id': None,
                    }

                '''
                Checking so as to produce ad after N no of posts
                '''
                
                try:
                    ad = ad_helper.filter_ads()
                    
                    if idx % 4 == 0 and idx != 0 and ad:
                        val = {
                            'id': ad.id,
                            'pictureGallery': '/media/posts/picture/'+ad.cover_image,
                            'poster_name': "",
                            'restaurant_id': "",
                            'description': "",
                            'type': 'AD',
                            'created_at': ad.created_at,
                            'updated_at': ad.updated_at
                        }
                        values.append(val)
                except BaseException as e:
                    '''
                    Quietly handle errors within the loop
                    '''
                    pass

                details = {
                    'id': post.id,
                    'pictureGallery': '/media/posts/picture/'+gallery.image,
                    'person_id': post.person_id,
                    'restaurant_id': post.restaurant_id,
                    'likes': liker.likes if liker.likes else 0,
                    'like_id': liker.id if liker.id else 0,
                    'like_person_id': liker.person_id if liker.person_id else 0,
                    'base64Pic': gallery.image,
                    'likes': post.likes,
                    'description': post.description,
                    'created_at': post.created_at,
                    'updated_at': post.updated_at,
                }
                values.append(details)
        data = {
            "data": values,
            "message": 'updated',
            "status_code": 200
        }

        return Response(data)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error:' + str(e),
        }
        return Response(error)
