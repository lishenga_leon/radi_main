import json

from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Count
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from authentication.api.decorators import check_token_status

from analytics.models import visits, Favourite
from person.models import person
from restaurant.models import Restaurant
from restaurant.serializers import RestaurantSerializers


class visitors():

    @api_view(['POST'])
    @check_token_status()
    def log_visit(request):
        """
        Log User visit to a particular restaurant
        -----
            {
                person_id:1,
                restaurant_id:1
            }
        """
        try:
            user = person.objects.get(id=request.data['person_id'])
            hotel = Restaurant.objects.get(id=request.data['restaurant_id'])

            visit = visits(
                restaurant=hotel,
                persona=user
            )
            visit.save()
            success = {
                'message': 'Success',
                'status_code': 200
            }

            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error :'+str(e),
            }
            return Response(error)

    @api_view(['POST'])
    @check_token_status()
    def add_favourite(request):
        """
        Add Restaurant Favourites
        -----
            {

                person_id:1,
                restaurant_id:1

            }
        """
        try:
            user = person.objects.get(id=request.data['person_id'])
            hotel = Restaurant.objects.get(
                id=request.data['restaurant_id'])

            visit = Favourite(
                restaurant=hotel,
                persona=user
            )
            visit.save()
            success = {
                'message': 'Success',
                'status_code': 200
            }

            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error :'+str(e),
            }
            return Response(error)

    @api_view(['POST'])
    @check_token_status()
    def top5_visits(request):
        """
        Get users top 5 visits
        -----
            {
                person_id:1,
            }
        """
        try:
            persona = person.objects.get(id=request.data['person_id'])
            logged_visits = visits.objects.filter(
                persona=persona).values('restaurant').annotate(total=Count('restaurant')).order_by('-total')[:5]
            favourites = Favourite.objects.filter(
                persona=persona).values('restaurant').annotate(total=Count('restaurant')).order_by('-total')
            # from pprint import pprint
            # pprint(vars(logged_visits))
            # data = json.dumps(list(logged_visits), cls=DjangoJSONEncoder)
            data = helpers._fech_restaurant(list(logged_visits))
            favs = helpers._fech_restaurant(list(favourites))

            success = {
                'data': data + favs,
                'message': 'Success',
                'status_code': 200
            }

            return Response(success)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error'+str(e),
            }

            return Response(error)


class helpers():
    def _fech_restaurant(restaurants):
        data = []
        for restaurant in restaurants:
            result = Restaurant.objects.get(id=restaurant['restaurant'])
            data.append(RestaurantSerializers(result).data)
        return data
