from django.urls import include, path
from . import controller

urlpatterns = [

    path('analytics/logVisit/', controller.visitors.log_visit),
    path('analytics/getTop5Favourites/', controller.visitors.top5_visits),
    path('analytics/addFavouriteRestaurant', controller.visitors.add_favourite),
    path('', include('configurations.urls')),

]
