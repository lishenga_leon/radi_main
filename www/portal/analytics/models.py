from django.db import models
from django.utils import timezone

from person.models import person
from restaurant.models import Restaurant


class visits(models.Model):
    """
    This tables logs all user visits to a particular restaurant
    """
    id = models.AutoField(primary_key=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    persona = models.ForeignKey(person, on_delete=models.CASCADE)
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(visits, self).save(*args, **kwargs)

class Favourite(models.Model):
    """
    Contains persons favourite restaurants
    """
    id = models.AutoField(primary_key=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    persona = models.ForeignKey(person, on_delete=models.CASCADE)
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField()
    
    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(visits, self).save(*args, **kwargs)