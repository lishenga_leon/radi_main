# Generated by Django 2.2 on 2019-06-26 06:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0005_auto_20190626_0601'),
    ]

    operations = [
        migrations.RenameField(
            model_name='payment',
            old_name='p',
            new_name='person',
        ),
    ]
