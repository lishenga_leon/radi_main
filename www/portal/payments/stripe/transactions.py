from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from payments.models import payment
from person.models import person
from payments.help import helpers
from authentication.api.decorators import check_token_status
from payments.serializers import PaymentsSerializer
import datetime, requests
from django.core.paginator import Paginator
from django.conf import settings
import datetime, pytz


PAYMENTSERVICE = settings.PAYMENTSERVICE
HEADERS = settings.HEADERS

#initiate a card charge via stripe on a customer card
@api_view(['POST'])
@check_token_status()
def create_stripe_customer_charge(request): 
    """
    Charge Card for Stripe check out

    -----
        {
            person_id:1,
            amount:50,
            currency:USD
            token: jdjaljkkfjksf, 
            order_id: int
        }
      
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':  
            person_id = request.data['person_id'] 
            persons = person.objects.get(id = person_id)
            currency = request.data['currency']

            payload = {
                "stripe_id":persons.stripe_id,
                'amount':request.data['amount'],
                'currency':currency
            }
            r = requests.post(PAYMENTSERVICE+'/transactions/createstripecharge/', data=payload, headers=HEADERS)
            data = r.json()

            if data['data']['status'] == "succeeded":
                transaction = helpers.create_visa_transaction(
                    person_id = person_id,
                    currency = currency, 
                    amount = request.data['amount'],
                    payment_mode = persons.card_brand,
                    payment_reference = data['data']['id'],
                    order_id = request.data['order_id']
                )

                transaction_data = {
                    "id":transaction.id,
                    "person_id":person_id,
                    "currency":transaction.currency,
                    "amount":transaction.amount
                } 

                success={
                    'message':'success',
                    "transaction_data":transaction_data,
                    'status_code':200
                }
                return Response(success)

            else:
                error={
                    'message':'could not charge account',
                    'data':[],
                    'status_code':500
                }

                return Response(error) 
    except BaseException as e:
        
        error={
            'data':[],
            'message':'error'+ str(e),
            'status_code':500
        }

        return Response(error)  


#add existing person card   
@api_view(['POST'])
@check_token_status()
def add_person_card(request):    
    """
    Create Stripe Person Card
    -----
        {
            person_id:1,
            number:424242424242424,
            exp_month:07,
            exp_year:22,
            cvc:494,
            token:nsfkjskjd
        }

    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':
            user_id = request.data['person_id'] 
            persons = person.objects.get(id=user_id)
            payload = {
                "number": request.data['number'] ,
                "exp_month": request.data['exp_month'],
                "exp_year": request.data['exp_year'],
                "cvc": request.data['cvc'],
                "stripe_id": persons.stripe_id
            }
            r = requests.post(PAYMENTSERVICE+'/registration/addpersoncard/', data=payload, headers=HEADERS)
            data = r.json()

            persons.card_last_four = data['data']['last4']
            persons.card_brand = data['data']['brand']
            person.date_card_added = datetime.datetime.now(tz=pytz.UTC)
            persons.save()
            
            success={
                "data":data['data'],
                "message":"success",
                "status_code":200
            } 
            return Response(success)  

    except BaseException as e:

        error={
            'data':[],
            'message':'error'+ str(e),
            'status_code':500
        }

        return Response(error) 


#Display all the customer cards for a user
@api_view(['POST'])
@check_token_status()
def get_customer_cards(request):
    """
    Display all the customer cards for a user
    -----
        {
            stripe_id:cus_asfhajho13
            token: jdjaljkkfjksf
        }
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':   
            payload = {
                "stripe_id": request.data['stripe_id']
            }
            r = requests.post(PAYMENTSERVICE+'/registration/getpersoncards/', data=payload, headers=HEADERS)
            data = r.json()

            success={
                'message':'success',
                'data':data['data'],
                'status_code':200,
            }
            return Response(success)

    except BaseException as e:
        error={
            'status_code':500,
            'message':'error' + str(e),
            'data':{
               
            }
        }
        return Response(error)


#Get the stripe balance for the stripe account
@api_view(['POST'])
@check_token_status()
def get_stripe_balance(request): 
    """
    Get the stripe balance for the stripe account
    -----
        {
            token: jdjaljkkfjksf
        }
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':   
            r = requests.get(PAYMENTSERVICE+'/transactions/getstripebalance/', headers=HEADERS)
            data = r.json()

            success={
                'message':'success',
                'data':data['data'],
                'status_code':200,
            }
            return Response(success) 

    except BaseException as e:

        error={
            'status_code':500,
            'message':'error:'+ str(e),
            'data':{}
        }
        return Response(error)
        

#Check person stripe details
@api_view(['POST'])
@check_token_status()
def check_person_stripe_details(request): 
    """
    Check person stripe details
    -----
        {
            stripe_id:cus_asfhajho13
            token: jdjaljkkfjksf
        }
    """

    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST': 
            payload = {
                "stripe_id": request.data['stripe_id']
            }
            r = requests.post(PAYMENTSERVICE+'/transactions/checkpersonstripedetails/', data=payload, headers=HEADERS)
            data = r.json()

            success={
                "data":data['data'],
                "message":"success",
                "status_code":200
                } 
            return Response(success) 

    except BaseException as e:

        error={
            'status_code':500,
            'message':'error:'+ str(e),
            'data':{}
        }
        return Response(error)

#initiate a stripe payout to a specific bank linked to the stripe account.
@api_view(['POST'])
@check_token_status()
def stripe_payout(request): 
    """
    Create Stripe Payout

    -----
        {
            amount:50,
            currency:USD,
            accountId_or_cardNo: 3434545,
            token: lkjdsjl
        }
      
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':  
            payload = {
                "amount": request.data['amount'],
                "currency": request.data['currency'],
                "accountId_or_cardNo": request.data['accountId_or_cardNo'],
            }
            r = requests.post(PAYMENTSERVICE+'/transactions/stripepayout/', data=payload, headers=HEADERS)
            data = r.json()

            success={
                "data":data['data'],
                "message":"success",
                "status_code":200
                } 
            return Response(success)   

    except BaseException as e:

        error={
            'status_code':500,
            'message':'error:'+ str(e),
            'data':{}
        }
        return Response(error)

