from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from payments.models import payment, calculate
from restaurant.models import Restaurant
from person.models import person
from bill.models import Order, OrderDetails
from payments.help import helpers
from django.core.paginator import Paginator
from payments.serializers import PaymentsSerializer
from bill.serializers import OrdersSerializers, OrderDetailsSerializers
from django.conf import settings
from authentication.api.decorators import check_token_status
import datetime
import pytz
import paypalrestsdk
import json
import requests


class AllModes():

    # Get all transactions for a particular restaurant
    @api_view(['POST'])
    @check_token_status()
    def get_all_restaurant_transactions(request):
        """
        Get all transactions for a restaurant

        -----
            {
                restaurant_id:1,
            }

        """
        try:
            if request.method == 'GET':
                snippets = 'success'
                return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'POST':
                data = []
                rest = Restaurant.objects.get(id=request.data['restaurant_id'])
                orders = Order.objects.filter(restaurant=rest)
                for ordering in orders:
                    details = OrderDetails.objects.get(order=ordering)
                    pay = payment.objects.get(order=ordering)
                    val = {
                        'orderdetails': OrderDetailsSerializers(details).data,
                        'order': OrdersSerializers(ordering).data,
                        'payments':  PaymentsSerializer(pay).data
                    }
                    data.append(val)
                success = {
                    'message': 'success',
                    'status_code': 200,
                    'data': data
                }
                return Response(success)
        except BaseException as e:

            error = {
                'data': [],
                'message': 'error' + str(e),
                'status_code': 500
            }

            return Response(error)

    # get one particular users payment for RADI mobile app
    @api_view(['POST'])
    @check_token_status()
    def get_particular_user_transactions_for_app(request):
        """
        Get one particular users payment
        -----
            {
                person_id:1
                page:1
                items: 5
            }
        """
        try:
            if request.method == 'GET':
                success = {'message': 'method not allowed', 'status_code': 401}
                return Response(success)

            elif request.method == 'POST':
                person_id = request.data['person_id']
                page = request.data['page']
                transact = payment.objects.filter(
                    person_id=person_id).order_by('-updated_at')
                paginator = Paginator(transact, 10)
                details = []
                for transaction in paginator.page(page):
                    values = {
                        'transactions': PaymentsSerializer(transaction).data,
                        'order': transaction.Orderdetails(),
                    }
                    details.append(values)

                data = {
                    'data': details,
                    'message': 'success',
                    'status_code': 200
                }

                return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
                'data': {}
            }
            return Response(error)

    # get one particular users payment
    @api_view(['POST'])
    @check_token_status()
    def get_all_transactions(request):
        """
        Get all transactions
        -----
            {
                page:1
                items: 5
                token: jdjaljkkfjksf
            }
        """
        try:
            if request.method == 'GET':
                success = {'message': 'method not allowed', 'status_code': 401}
                return Response(success)

            elif request.method == 'POST':
                person_id = request.data['person_id']
                page = request.GET.get('page', request.data['page'])
                transact = payment.objects.all().order_by('-updated_at')
                paginator = Paginator(transact, request.data['items'])
                details = []
                for transaction in paginator.page(page):
                    values = {
                        'transactions': PaymentsSerializer(transaction).data,
                        'order': transaction.Orderdetails(),
                    }
                    details.append(values)

                data = {
                    'data': details,
                    'message': 'success',
                    'status_code': 200
                }

                return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
                'data': {}
            }
            return Response(error)

    # get transactions between a certain period
    @api_view(['POST'])
    @check_token_status()
    def get_all_transactions_specific_period(request):
        """
        Get all transactions between a certain period
        -----
        
            {
                start: datetime
                end: datetime
                restaurant_id: int,
                page: int,
                items: int,
                status: succeeded | PENDING | served
            }
        """
        try:
            if request.method == 'GET':
                success = {'message': 'method not allowed', 'status_code': 401}
                return Response(success)

            elif request.method == 'POST':
                restaurant = Restaurant.objects.get(
                    id=request.data['restaurant_id'])

                orders = Order.objects.filter(restaurant=restaurant).filter(
                    status__icontains=request.data['status']).filter(created_at__range=(
                        request.data['start'], request.data['end']))

                page = request.data['page']

                paginator = Paginator(orders, request.data['items'])
                details = []
                for order in paginator.page(page):
                    transact = payment.objects.filter(order=order).get()
                    details.append(transact.timeddetails(request))

                data = {
                    'data': details,
                    'message': 'success',
                    'status_code': 200
                }

                return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
                'data': {}
            }
            return Response(error)


    # Calculate total transactions between a certain period
    @api_view(['POST'])
    @check_token_status()
    def calculate_total_transactions_specific_period(request):
        """
        Calculate total transactions between a certain period
        -----
            {
                start: datetime
                end: datetime
                restaurant_id: int,
            }
        """
        try:
            if request.method == 'GET':
                success = {'message': 'method not allowed', 'status_code': 401}
                return Response(success)

            elif request.method == 'POST':
                restaurant = Restaurant.objects.get(
                    id=request.data['restaurant_id'])

                orders = Order.objects.filter(restaurant=restaurant).filter(
                    status__icontains='succeeded').filter(created_at__range=(
                        request.data['start'], request.data['end']))
                details = []
                cal = calculate(
                    restaurant = restaurant,
                    total_price = 0,
                    start_time = request.data['start'],
                    end_time = request.data['end'],
                    created_at=datetime.datetime.now(tz=pytz.timezone('Africa/Nairobi')),
                    updated_at=datetime.datetime.now(tz=pytz.timezone('Africa/Nairobi'))
                )
                cal.save()
                for order in orders:
                    transact = payment.objects.filter(order=order).get()
                    total_amount = calculate.objects.filter(restaurant = restaurant).filter(start_time = request.data['start']).filter(end_time = request.data['end']).get()
                    total_amount.total_price = total_amount.total_price + transact.amount
                    total_amount.save()

                total_amount = calculate.objects.filter(restaurant = restaurant).filter(start_time = request.data['start']).filter(end_time = request.data['end']).get()
                data = {
                    'data': {
                        'total_amount': total_amount.total_price,
                        'currency': 'USD'
                    },
                    'message': 'success',
                    'status_code': 200
                }
                total_amount.delete()

                return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
                'data': {}
            }
            return Response(error)


    # Search transaction by transaction reference
    @api_view(['POST'])
    @check_token_status()
    def search_transaction_by_transaction_reference(request):
        """
        Search transaction by transaction reference
        -----
            {
                transaction_reference: KHDJKAY463
                page:1
                items: 5
            }
        """
        try:
            if request.method == 'GET':
                success = {'message': 'method not allowed', 'status_code': 401}
                return Response(success)

            elif request.method == 'POST':
                transact = payment.objects.filter(transaction_reference__icontains=request.data['transaction_reference'])

                page = request.data['page']
                paginator = Paginator(transact, request.data['items'])

                details = []

                for transaction in paginator.page(page):
                    values = {
                        'transactions': PaymentsSerializer(transaction).data,
                        'order': transaction.Orderdetails(),
                    }
                    details.append(values)

                data = {
                    'data': details,
                    'message': 'success',
                    'status_code': 200
                }

                return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
                'data': {}
            }
            return Response(error)

    # get one particular users payment for Radi portal
    @api_view(['POST'])
    @check_token_status()
    def get_particular_user_transactions_for_portal(request):
        """
        Get one particular users payment for portal
        -----
            {
                person_id:1
                page:1
                items: 5
                start: datetime
                end: datetime
                status: succeeded | PENDING | served
            }
        """
        try:
            if request.method == 'GET':
                success = {'message': 'method not allowed', 'status_code': 401}
                return Response(success)

            elif request.method == 'POST':

                person_id = request.data['person_id']
                page = request.data['page']
                orders = Order.objects.filter(person_id=person_id).filter(
                    status__icontains=request.data['status']).filter(created_at__range=(
                        request.data['start'], request.data['end']))

                paginator = Paginator(orders, request.data['items'])
                details = []
                for order in paginator.page(page):
                    transact = payment.objects.filter(order=order).get()
                    details.append(transact.timeddetails(request))

                data = {
                    'data': details,
                    'message': 'success',
                    'status_code': 200
                }

                return Response(data)

        except BaseException as e:

            error = {
                'status_code': 500,
                'message': 'error:' + str(e),
                'data': {}
            }
            return Response(error)
