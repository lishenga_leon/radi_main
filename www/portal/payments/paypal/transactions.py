from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from payments.models import payment
from person.models import person
from payments.help import helpers
from authentication.api import views
from payments.serializers import PaymentsSerializer
from django.core.paginator import Paginator
from authentication.api.decorators import check_token_status
from django.conf import settings
import datetime
import pytz
import paypalrestsdk
import json
import requests
from django.shortcuts import render, HttpResponse, redirect


PAYMENTSERVICE = settings.PAYMENTSERVICE
HEADERS = settings.HEADERS

PAYPAL_CLIENT_ID = settings.PAYPAL_CLIENT_ID
PAYPAL_MODE = settings.PAYPAL_MODE
PAYPAL_CLIENT_SECRET = settings.PAYPAL_CLIENT_SECRET

paypalrestsdk.configure({
    "mode": PAYPAL_MODE,  # sandbox or live
    "client_id": PAYPAL_CLIENT_ID,
    "client_secret": PAYPAL_CLIENT_SECRET
})


class Paypal():

    # initiate a card charge via paypal on a customer card
    @api_view(['GET'])
    def paypalUI(self, request):
        """
        Paypal UI

        -----

        """
        try:
            if request.method == 'POST':
                snippets = 'success'
                return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'GET':
                return render(request, 'paypal/index.html')
        except BaseException as e:

            error = {
                'data': [],
                'message': 'error' + str(e),
                'status_code': 500
            }

            return render(request, 'paypal/cancelled.html')

    # initiate a paypal transaction
    @api_view(['POST'])
    def create_paypal_customer_charge(self, request):
        """
        Charge Card for PayPal check out

        -----
            {
                person_id:1,
                totalFoodPrice:int,
                restaurant_id: int
            }

        """
        try:
            if request.method == 'GET':
                snippets = 'success'
                return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'POST':
                totalFoodPrice = request.POST.get('totalFoodPrice')
                person_id = request.POST.get('person_id')
                restaurant_id = request.POST.get('restaurant_id')

                payment = paypalrestsdk.Payment({
                    "intent": "sale",
                    "payer": {
                        "payment_method": "paypal"
                    },
                    "redirect_urls": {
                        "return_url": "http://127.0.0.1:8000/paypal/transactionResult/?person_id="+person_id+"&restaurant_id="+restaurant_id+"&totalFoodPrice="+totalFoodPrice,
                        "cancel_url": "http://127.0.0.1:8000/paypal/transactionCancelled/"
                    },
                    "transactions":
                    [{
                        "item_list": {
                            "items":
                            [{
                                "name": "food",
                                "sku": "food",
                                "price": totalFoodPrice,
                                "currency": "USD",
                                "quantity": 1
                            }]
                        },
                        "amount": {
                            "total": totalFoodPrice,
                            "currency": "USD"
                        },
                        "description": "This is the payment transaction description."
                    }]
                })

                if payment.create():

                    data = {}
                    for pay in payment.links:
                        if (pay.method == 'REDIRECT'):
                            val = {
                                'href': pay.href
                            }
                            data = val
                            break

                    helpers.track_paypal_transaction(
                        person_id=person_id,
                        restaurant_id=restaurant_id,
                        totalFoodPrice=totalFoodPrice
                    )

                    return redirect(data['href'])

                else:

                    return render(request, 'paypal/cancelled.html')

        except BaseException as e:

            error = {
                'data': [],
                'message': 'error' + str(e),
                'status_code': 500
            }

            return render(request, 'paypal/cancelled.html')

    # method that's a callback when a paypal transaction is undergoing
    @api_view(['GET'])
    def transaction_result(self, request):
        """
        Transaction result fron paypal

        -----
            {
                person_id:1,
                totalFoodPrice:int,
                restaurant_id: int
            }

        """
        try:
            if request.method == 'POST':
                snippets = 'success'
                return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'GET':
                payerId = request.GET.get('PayerID')
                paymentId = request.GET.get('paymentId')
                totalFoodPrice = request.GET.get('totalFoodPrice')
                person_id = request.GET.get('person_id')
                restaurant_id = request.GET.get('restaurant_id')

                payment = paypalrestsdk.Payment.find(paymentId)

                if payment.execute({"payer_id": payerId}):

                    data = {}
                    for pay in payment.transactions:
                        for final in pay.related_resources:
                            if(final.sale.state == 'completed'):
                                val = {
                                    'transaction_id': final.sale.id
                                }
                                data = val
                                break
                    helpers.update_tracked_paypal_transaction(
                        paypal_payer_id=payment.payer.payer_info.payer_id,
                        paypal_payment_id=payment.id,
                        paypal_payer_email=payment.payer.payer_info.email,
                        paypal_transaction_id=data['transaction_id'],
                        person_id=person_id,
                        restaurant_id=restaurant_id,
                        totalFoodPrice=totalFoodPrice
                    )
                    return render(request, 'paypal/success.html')
                else:
                    return render(request, 'paypal/cancelled.html')
        except BaseException as e:

            error = {
                'data': [],
                'message': 'error' + str(e),
                'status_code': 500
            }
            return render(request, 'paypal/cancelled.html')

    # paypal transaction has been cancelled
    @api_view(['GET'])
    def transaction_cancelled(self, request):
        """
        Paypal transaction cancelled

        -----

        """
        try:
            if request.method == 'POST':
                snippets = 'success'
                return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'GET':
                return render(request, 'paypal/cancelled.html')
        except BaseException as e:

            error = {
                'data': [],
                'message': 'error' + str(e),
                'status_code': 500
            }

            return render(request, 'paypal/cancelled.html')

    # finish a paypal transaction
    @api_view(['POST'])
    @check_token_status()
    def complete_paypal_transaction(self, request):
        """
        Complete paypal transaction

        -----
            {
                person_id:1,
                amount:int,
                restaurant_id: int,
                order_id: int
            }

        """
        try:
            if request.method == 'GET':
                snippets = 'success'
                return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'POST':
                transaction = helpers.complete_paypal_transaction(
                    person_id=request.data['person_id'],
                    amount=request.data['amount'],
                    restaurant_id=request.data['restaurant_id'],
                    order_id=request.data['order_id']
                )

                transaction_data = {
                    "id": transaction.id,
                    "person_id": request.data['person_id'],
                    "currency": transaction.currency,
                    "amount": transaction.amount
                }

                success = {
                    'message': 'success',
                    "transaction_data": transaction_data,
                    'status_code': 200
                }
                return Response(success)
        except BaseException as e:

            error = {
                'data': [],
                'message': 'error' + str(e),
                'status_code': 500
            }
            return Response(error)

    # finish a paypal transaction
    @api_view(['POST'])
    @check_token_status()
    def get_all_paypal_transactions(self, request):
        """
        Get all paypal transactions from paypal

        -----
            {
                items:int,
            }

        """
        try:
            if request.method == 'GET':
                snippets = 'success'
                return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'POST':
                payment_history = paypalrestsdk.Payment.all(
                    {"count": request.data['items']})
                success = {
                    'message': 'success',
                    'status_code': 200
                }
                return Response(success)
        except BaseException as e:

            error = {
                'data': [],
                'message': 'error' + str(e),
                'status_code': 500
            }
            return Response(error)
