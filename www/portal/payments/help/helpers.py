from payments.models import payment, track_paypal
from bill.models import Order
from person.models import person
import datetime, pytz, random, string

#store visa transaction details in the system
def create_visa_transaction(person_id, amount, currency, payment_reference, payment_mode, order_id):
    #status = 1 means paid

    # transaction reference
    size=20 
    chars=string.ascii_uppercase + string.digits
    newName = ''.join(random.choice(chars) for _ in range(size))

    # successful pay of order
    order = Order.objects.get(id = order_id)
    order.status = "succeeded"
    order.save()

    persons = person.objects.get(id = person_id)

    transaction = payment(
        transaction_reference = newName, 
        person=persons,
        amount= amount, 
        currency= currency,
        status=1,
        paypal_payer_email = 'NOT APPLICABLE',
        paypal_payment_id = 'NOT APPLICABLE',
        paypal_payer_id = 'NOT APPLICABLE',
        payment_reference = payment_reference,
        payment_mode = payment_mode,
        created_at = datetime.datetime.now(tz=pytz.UTC), 
        updated_at= datetime.datetime.now(tz=pytz.UTC),
        order = order
    )

    transaction.save()

    return transaction

# track paypal transaction
def track_paypal_transaction(person_id, restaurant_id, totalFoodPrice):

    track = track_paypal(
        person_id=person_id,
        restaurant_id= restaurant_id, 
        food_price = totalFoodPrice,
        paypal_payer_id = '',
        paypal_payer_email = '',
        paypal_payment_id = '',
        paypal_transaction_id = '',
        created_at = datetime.datetime.now(tz=pytz.UTC), 
        updated_at= datetime.datetime.now(tz=pytz.UTC)
    )

    track.save()

    return track


# update tracked paypal transaction
def update_tracked_paypal_transaction(person_id, restaurant_id, totalFoodPrice, paypal_payer_email, paypal_payment_id, paypal_transaction_id, paypal_payer_id):

    track = track_paypal.objects.filter(person_id=person_id).filter(restaurant_id=restaurant_id).filter(food_price=totalFoodPrice).latest('created_at')
    track.paypal_payer_id = paypal_payer_id
    track.paypal_payer_email = paypal_payer_email
    track.paypal_payment_id = paypal_payment_id
    track.paypal_transaction_id = paypal_transaction_id
    track.save()

    return track

#store paypal transaction details in the system
def complete_paypal_transaction(person_id, amount, restaurant_id, order_id):
    #status = 1 means paid

    track = track_paypal.objects.filter(person_id=person_id).filter(restaurant_id=restaurant_id).filter(food_price=amount).latest('created_at')

    # transaction reference
    size=20 
    chars=string.ascii_uppercase + string.digits
    newName = ''.join(random.choice(chars) for _ in range(size))

    # successful pay of order
    order = Order.objects.get(id = order_id)
    order.status = "succeeded"
    order.save()

    persons = person.objects.get(id = person_id)

    transaction = payment(
        transaction_reference = newName, 
        person = persons,
        amount = amount, 
        currency = 'USD',
        status = 1,
        paypal_payer_email = track.paypal_payer_email,
        paypal_payment_id = track.paypal_payment_id,
        paypal_payer_id = track.paypal_payer_id,
        payment_reference = track.paypal_transaction_id,
        payment_mode = 'PAYPAL',
        created_at = datetime.datetime.now(tz=pytz.UTC), 
        updated_at = datetime.datetime.now(tz=pytz.UTC),
        order = order
    )

    transaction.save()

    return transaction

