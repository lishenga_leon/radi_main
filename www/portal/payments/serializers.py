from rest_framework.serializers import ModelSerializer, Serializer, SerializerMethodField
from .models import payment

class PaymentsSerializer(ModelSerializer):
    # restaurant = RestaurantSerializers()
    class Meta:
        model = payment
        fields = '__all__'
