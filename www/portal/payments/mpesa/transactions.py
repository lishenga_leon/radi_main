from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from payments.models import payment
from person.models import person
from payments.help import helpers
from authentication.api.decorators import check_token_status
import datetime
import requests
from django.core.paginator import Paginator
from django.conf import settings


PAYMENTSERVICE = settings.PAYMENTSERVICE
HEADERS = settings.HEADERS

#Initiate a push STK charge via mpesa
@api_view(['POST'])
@check_token_status()
def createPushStkCharge(request): 
    """
    Initiate a push STK charge via mpesa

    -----
        {
            person_id:1,
            amount:50,
            msisdn:0791172530,
            currency:KES
            token: jdjaljkkfjksf
        }
      
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':   
            person_id = request.data['person_id'] 
            persons = person.objects.get(id = person_id)
            currency = request.data['currency']

            payload = {
                "msisdn":request.data['msisdn'],
                'amount':request.data['amount'],
            }
            r = requests.post(PAYMENTSERVICE+'/transaction/PushStkc2b/', data=payload, headers=HEADERS)
            data = r.json()

            
            return Response(data)

    except BaseException as e:
        
        error={
            'data':[],
            'message':'error'+ str(e),
            'status_code':500
        }

        return Response(error)  

#Initiate a bc2 charge via mpesa
@api_view(['POST'])
@check_token_status()
def createb2cCharge(request): 
    """
    Initiate a bc2 charge via mpesa

    -----
        {
            person_id:1,
            amount:50,
            msisdn:0791172530,
            currency:KES
            token: jdjaljkkfjksf
        }
      
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':
            person_id = request.data['person_id'] 
            persons = person.objects.get(id = person_id)
            currency = request.data['currency']

            payload = {
                "msisdn":request.data['msisdn'],
                'amount':request.data['amount'],
            }
            r = requests.post(PAYMENTSERVICE+'/transaction/b2c/', data=payload, headers=HEADERS)
            data = r.json()

            
            return Response(data)


    except BaseException as e:
        
        error={
            'data':[],
            'message':'error'+ str(e),
            'status_code':500
        }

        return Response(error) 


@api_view(['POST'])
@check_token_status()
def accountbalance(request): 
    """
    Initiate a bc2 charge via mpesa

    -----
        {
            person_id:1,
            token: jdjaljkkfjksf
        }
      
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST': 
            person_id = request.data['person_id'] 
            persons = person.objects.get(id = person_id)

            r = requests.get(PAYMENTSERVICE+'/transaction/accountBalance/', headers=HEADERS)
            data = r.json()

            return Response(data)

    except BaseException as e:
        
        error={
            'data':[],
            'message':'error'+ str(e),
            'status_code':500
        }

        return Response(error) 

