from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from payments.models import payment
from person.models import person
from payments.help import helpers
from authentication.api import views
import datetime
import requests
from django.core.paginator import Paginator
from django.conf import settings


"""
Callback function for b2c transaction from Mpesa API.
$callbackJSONData=file_get_contents('php://input');
        $callbackData=json_decode($callbackJSONData);
        $resultCode=$callbackData->Result->ResultCode;
        $resultDesc=$callbackData->Result->ResultDesc;
        $originatorConversationID=$callbackData->Result->OriginatorConversationID;
        $conversationID=$callbackData->Result->ConversationID;
        $transactionID=$callbackData->Result->TransactionID;
        $transactionReceipt=$callbackData->Result->ResultParameters->ResultParameter[0]->Value;
        $transactionAmount=$callbackData->Result->ResultParameters->ResultParameter[1]->Value;
        $b2CWorkingAccountAvailableFunds=$callbackData->Result->ResultParameters->ResultParameter[2]->Value;
        $b2CUtilityAccountAvailableFunds=$callbackData->Result->ResultParameters->ResultParameter[3]->Value;
        $transactionCompletedDateTime=$callbackData->Result->ResultParameters->ResultParameter[4]->Value;
        $receiverPartyPublicName=$callbackData->Result->ResultParameters->ResultParameter[5]->Value;
        $B2CChargesPaidAccountAvailableFunds=$callbackData->Result->ResultParameters->ResultParameter[6]->Value;
        $B2CRecipientIsRegisteredCustomer=$callbackData->Result->ResultParameters->ResultParameter[7]->Value;


        $result=[
          "resultCode"=>$resultCode,
          "resultDesc"=>$resultDesc,
            "originatorConversationID"=>$originatorConversationID,
            "conversationID"=>$conversationID,
            "transactionID"=>$transactionID,
            "transactionReceipt"=>$transactionReceipt,
            "transactionAmount"=>$transactionAmount,
            "b2CWorkingAccountAvailableFunds"=>$b2CWorkingAccountAvailableFunds,
            "b2CUtilityAccountAvailableFunds"=>$b2CUtilityAccountAvailableFunds,
            "transactionCompletedDateTime"=>$transactionCompletedDateTime,
            "receiverPartyPublicName"=>$receiverPartyPublicName,
            "B2CChargesPaidAccountAvailableFunds"=>$B2CChargesPaidAccountAvailableFunds,
            "B2CRecipientIsRegisteredCustomer"=>$B2CRecipientIsRegisteredCustomer
        ];

        return json_encode($result);

"""

@api_view(['POST'])
def bc2callback(request):

    print(request)
    success={
        'message':'success',
        "transaction_data":transaction_data,
        'status_code':200
    }
    return Response(success)
