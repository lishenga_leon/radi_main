from django.urls import include, path
from payments.stripe import transactions
from payments.ForAllModes import transactions as forallmodes
from payments.paypal import transactions as paypalPay
from payments.mpesa import transactions as mpesapay

urlpatterns = [
 
    #Stripe routes
    path('stripe/createstripecharge/', transactions.create_stripe_customer_charge),
    path('stripe/getpersoncards/', transactions.get_customer_cards),
    path('stripe/addpersoncard/', transactions.add_person_card),
    path('stripe/getstripebalance/', transactions.get_stripe_balance),
    path('stripe/stripepayout/', transactions.stripe_payout),
    path('stripe/checkpersonstripedetails/', transactions.check_person_stripe_details),


    #Paypal routes
    path('paypal/UI/', paypalPay.Paypal.paypalUI),
    path('paypal/UI/paypal/createPayPalCharge/', paypalPay.Paypal.create_paypal_customer_charge),
    path('paypal/transactionResult/', paypalPay.Paypal.transaction_result),
    path('paypal/transactionCancelled/', paypalPay.Paypal.transaction_cancelled),
    path('paypal/completePayPalTransaction/', paypalPay.Paypal.complete_paypal_transaction),
    path('paypal/getPayPalTransactionsFromPaypal/', paypalPay.Paypal.get_all_paypal_transactions),


    #Payment routes
    path('payments/getparticularPersontransactionsForApp/', forallmodes.AllModes.get_particular_user_transactions_for_app),
    path('payments/getAllTransactions/', forallmodes.AllModes.get_all_transactions),
    path('payments/getAllTransactionsForRestaurant/', forallmodes.AllModes.get_all_restaurant_transactions),
    path('payments/getAllTransactionsForParticularTimePeriod/', forallmodes.AllModes.get_all_transactions_specific_period),
    path('payments/calculateTransactionsForParticularTimePeriod/', forallmodes.AllModes.calculate_total_transactions_specific_period),
    path('payments/searchByTransactionReference/', forallmodes.AllModes.search_transaction_by_transaction_reference),
    path('payments/getPersonTransactionsForPortal/', forallmodes.AllModes.get_particular_user_transactions_for_portal),


    #Food routes
    path('', include('food.urls')),

    #Mpesa Routes
    path('mpesa/PushStkCharge/', mpesapay.createPushStkCharge),
    path('mpesa/b2cCharge/', mpesapay.createb2cCharge),
    path('mpesa/accountbalance/', mpesapay.accountbalance),

]