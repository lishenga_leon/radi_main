from django.db import models
from bill.models import Order, OrderDetails
from restaurant.models import Restaurant
from person.models import person

# Create your models here.


class payment(models.Model):

    # payment reference is from stripe, mpesa or paypal. for paypal the payment reference is the transaction id
    # transaction reference is internal, system generates it.

    id = models.AutoField(primary_key=True)
    transaction_reference = models.CharField(max_length=255, default=None)
    payment_reference = models.CharField(max_length=255, default=None)
    amount = models.IntegerField(default=0)
    person = models.ForeignKey(person, on_delete=models.CASCADE, default=None)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, default=None)
    currency = models.CharField(max_length=500, default=None)
    status = models.IntegerField(default=0)
    payment_mode = models.CharField(max_length=255, default=None)
    paypal_payer_id = models.CharField(
        max_length=255, default='NOT APPLICABLE')
    paypal_payment_id = models.CharField(
        max_length=255, default='NOT APPLICABLE')
    paypal_payer_email = models.CharField(
        max_length=255, default='NOT APPLICABLE')
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

    def Orderdetails(self):
        det = {
            'id': self.order.id,
            'person': {
                'id': self.person.id,
                'fullname': self.person.fullname,
                'email': self.person.email,
                'msisdn': self.person.msisdn,
                'status': self.person.status,
                'stripe_id': self.person.stripe_id,
                'card_brand': self.person.card_brand,
                'card_last_four': self.person.card_last_four,
                'date_card_added': self.person.date_card_added,
                'created_at': self.person.created_at,
                'updated_at': self.person.updated_at
            },
            'restaurant': {
                'id': self.order.restaurant.id,
                'name': self.order.restaurant.name,
                'description': self.order.restaurant.description,
                'location': self.order.restaurant.location,
                'lat': self.order.restaurant.lat,
                'category': self.order.restaurant.category,
                'long': self.order.restaurant.long,
                'cover_image': '/media/restaurant/picture/'+self.order.restaurant.cover_image,
                'status': self.order.restaurant.status,
                'phone_number': self.order.restaurant.phone_number,
                'max_capacity': self.order.restaurant.max_capacity,
                'created_at': self.order.restaurant.created_at,
                'updated_at': self.order.restaurant.updated_at
            },
            'orderDetails': self.order.details(),
            'food_instructions': self.order.food_instructions,
            'status': self.order.status,
            'expected_time': self.order.expected_time,
            'order_type': self.order.order_type,
            'created_at': self.order.created_at,
            'updated_at': self.order.updated_at
        }
        return det

    def timeddetails(self, request):
        # rest = Restaurant.objects.get(id=request.data['restaurant_id'])
        # orders = Order.objects.filter(restaurant=rest)
        # data = []
        # for ordering in orders:
        # orderdet = OrderDetails.objects.get(order=self.order)
        det = {
            'id': self.id,
            'customer': {
                'name': self.person.fullname,
                'phone': self.person.msisdn,
            },
            'billInfo': {
                'data': self.order.details()
            },
            'amountPayed': self.amount,
            "modeOfPayment": self.payment_mode,
            "dateOfPayment": self.created_at
        }
        # data.append(det)
        return det

# track paypal transactions
class track_paypal(models.Model):

    id = models.AutoField(primary_key=True)
    person_id = models.IntegerField(default=0)
    restaurant_id = models.IntegerField(default=0)
    food_price = models.IntegerField(default=0)
    paypal_payer_id = models.CharField(max_length=255, default=None)
    paypal_payment_id = models.CharField(max_length=255, default=None)
    paypal_payer_email = models.CharField(max_length=255, default=None)
    paypal_transaction_id = models.CharField(max_length=255, default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

# used to calculate total transactions for a restaurant
class calculate(models.Model):

    id = models.AutoField(primary_key=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, default=None)
    total_price = models.IntegerField(default=0)
    start_time = models.DateTimeField(default=None)
    end_time = models.DateTimeField(default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)