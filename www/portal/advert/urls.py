from django.urls import include, path
from . import controller

urlpatterns = [
    path('advert/create/adType', controller.create_adtype),
    path('advert/update/adType', controller.update_adtype),
    path('advert/delete/adType', controller.delete_adtype),
    path('advert/get/adTypes', controller.get_adtypes),
    path('advert/create', controller.create_advert),
    path('advert/update', controller.update_advert),
    path('advert/delete', controller.delete_advert),
    path('advert/get', controller.get_ads),
]
