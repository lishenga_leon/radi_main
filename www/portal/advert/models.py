from django.db import models
from person.models import person
from django.utils import timezone
from django.db.models.aggregates import Count
import random

# Create your models here.


class AdvertType(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField(max_length=100)
    status = models.CharField(default='ACTIVE', max_length=100)
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(AdvertType, self).save(*args, **kwargs)


class Advert(models.Model):
    name = models.CharField(max_length=100)
    redirect_url = models.CharField(max_length=100)
    cover_image = models.CharField(max_length=100)
    ad_type = models.ForeignKey(AdvertType, on_delete=models.CASCADE)
    user = models.ForeignKey(person, on_delete=models.CASCADE)
    # """
    #     UNPAID|PAID|CANCELLED|SUSPENDED
    # """
    status = models.CharField(default='UNPAID', max_length=100)
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(Advert, self).save(*args, **kwargs)

    @classmethod
    def paid_ads_fetcher(cls):
        ads = cls.objects.filter('status','PAID')
        return ads

    @classmethod
    def random_sampler(cls, N=1):
        """Sample any iterable (like a Django QuerySet) to retrieve N random elements
    
        Arguments:
          qs (iterable): Any iterable (like a Django QuerySet)
          N (int): Number of samples to retrieve at random from the iterable
    
        References:
          @DZinX:  https://stackoverflow.com/a/12583436/623735
          @MartinPieters: https://stackoverflow.com/a/12581484/623735
        """
        qs = cls.paid_ads_fetcher()
        samples = []
        iterator = iter(qs)
        # Get the first `N` elements and put them in your results list to preallocate memory
        try:
            for _ in range(N):
                samples.append(iterator.next())
        except StopIteration:
            raise ValueError("N, the number of reuested samples, is larger than the length of the iterable.")
        random.shuffle(samples)  # Randomize your list of N objects
        # Now replace each element by a truly random sample
        for i, v in enumerate(qs, N):
            r = random.randint(0, i)
            if r < N:
                samples[r] = v  # at a decreasing rate, replace random items
        return samples[0]
    
