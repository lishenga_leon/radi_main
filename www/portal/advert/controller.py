from django.core.paginator import Paginator
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from authentication.api.decorators import check_token_status
from .models import AdvertType, Advert
from food.helpers import helper as food_helper
from .serializers import AdtypeSerializer, AdvertsSerializer
from person.models import person


@api_view(['POST'])
@check_token_status()
def create_adtype(request):
    """
    Create Ad Type
    ---
        {
            name:string,
            price:int
        }
    """
    adtype = AdvertType(name=request.data['name'], price=request.data['price'])
    adtype.save()
    success = {
        'message': 'Success',
        'status_code': 200
    }

    return Response(success)


@api_view(['POST'])
@check_token_status()
def update_adtype(request):
    """
    Update
    -----
        {
            type_id:int,
            name:string,
            price:int
        }
    """
    adtype = AdvertType.objects.filter(id=request.data['type_id'])
    adtype.name = request.data['name']
    adtype.price = request.data['price']
    adtype.save()
    success = {
        'message': 'Success',
        'status_code': 200
    }

    return Response(success)


@api_view(['DELETE'])
@check_token_status()
def delete_adtype(request):
    """
    Delete
    -----
        {
            type_id:int,
            name:string,
            price:int
        }
    """
    adtype = AdvertType.objects.filter(id=request.data['type_id'])
    adtype.delete()

    success = {
        'message': 'Success',
        'status_code': 200
    }

    return Response(success)


@api_view(['POST'])
@check_token_status()
def get_adtypes(request):
    adtypes = AdvertType.objects.all()
    types = AdtypeSerializer(adtypes, many=True)
    success = {
        'message': 'Success',
        'data': types.data,
        'status_code': 200,
    }

    return Response(success)


# Manage Adverts

@api_view(['POST'])
@check_token_status()
def create_advert(request):
    """
    Create Advert
    ----
        {
            name:string,
            redirect_url:string,
            cover_image:string,
            ad_type_id:integer,
            user_id:integer,
            picture_encoding:string,
        }
    """
    adtype = AdvertType.objects.filter(id=request.data['ad_type_id']).get()
    user = person.objects.filter(id=request.data['user_id']).get()

    decoder = food_helper.base64decoding(
        pictures=request.data['cover_image'],
        file_type=request.data['picture_encoding']
    )

    ad = Advert(
        name=request.data['name'],
        redirect_url=request.data['redirect_url'],
        cover_image=decoder['newName'],
        ad_type=adtype,
        user=user
    )
    ad.save()

    success = {
        'message': 'Success',
        'status_code': 200
    }

    return Response(success)


@api_view(['POST'])
@check_token_status()
def update_advert(request):
    """
    Update Advert
    ---
        {
            ad_id:integer
            name:string,
            redirect_url:string,
            cover_image:string,
            ad_type_id:integer,
            user_id:integer,
        }
    """
    adtype = AdvertType.objects.filter(id=request.data['ad_type_id']).get()
    user = person.objects.filter(id=request.data['user_id']).get()

    decoder = food_helper.base64decoding(
        pictures=request.data['cover_image'],
        file_type=request.data['picture_encoding']
    )

    ad = Advert.objects.filter(id=request.data['ad_id'])
    ad.name = request.data['name']
    ad.redirect_url = request.data['redirect_url']
    ad.cover_image = decoder.newName
    ad.ad_type = adtype
    ad.user = user
    ad.save()

    success = {
        'message': 'Success',
        'status_code': 200
    }

    return Response(success)


@api_view(['POST'])
@check_token_status()
def get_ads(request):
    user = person.objects.filter(id=request.data['user_id']).get()
    ads = Advert.objects.filter(user=user)
    types = AdvertsSerializer(ads, many=True)
    success = {
        'message': 'Success',
        'data': types.data,
        'status_code': 200,
    }

    return Response(success)


@api_view(['DELETE'])
@check_token_status()
def delete_advert(request):
    adtype = Advert.objects.filter(id=request.data['ad_id'])
    adtype.delete()

    success = {
        'message': 'Success',
        'status_code': 200
    }

    return Response(success)
