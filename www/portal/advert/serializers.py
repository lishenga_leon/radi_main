from rest_framework import serializers
from . import models


class AdtypeSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'name', 'price', 'created_at', 'updated_at')
        model = models.AdvertType

class AdvertsSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = models.Advert
    