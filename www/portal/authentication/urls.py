from django.urls import include, path
from authentication.api import views

urlpatterns = [

    path('authentication/getUserToken/', views.get_token),
]