from rest_framework import status
from rest_framework.response import Response
from authentication.models import token
import datetime
import string
import pytz
import random

# function for creating token using person ID for a person who wants to login the app
def create_token_person_id(person_id):
    size = 20
    chars = string.ascii_uppercase + string.digits
    tokenation = ''.join(random.choice(chars) for _ in range(size))
    if token.objects.filter(person_id=person_id).exists():
        token.objects.filter(person_id=person_id).delete()
        tokens = token(
            person_id=person_id,
            token=tokenation,
            device_id='NOT APPLICABLE',
            created_at=datetime.datetime.now(tz=pytz.UTC),
        )
        tokens.save()

        return tokens
    else:
        tokens = token(
            person_id=person_id,
            token=tokenation,
            device_id='NOT APPLICABLE',
            created_at=datetime.datetime.now(tz=pytz.UTC),
        )
        tokens.save()

        return tokens


# function for creating token using unique device ID for a person who doesn't wants to login the app
def create_token_device_id(device_id):
    size = 20
    chars = string.ascii_uppercase + string.digits
    tokenation = ''.join(random.choice(chars) for _ in range(size))
    if token.objects.filter(device_id=device_id).exists():
        token.objects.filter(device_id=device_id).delete()
        tokens = token(
            person_id='NOT APPLICABLE',
            token=tokenation,
            device_id= device_id,
            created_at=datetime.datetime.now(tz=pytz.UTC),
        )
        tokens.save()

        return tokens
    else:
        tokens = token(
            person_id='NOT APPLICABLE',
            token=tokenation,
            device_id= device_id,
            created_at=datetime.datetime.now(tz=pytz.UTC),
        )
        tokens.save()

        return tokens


# function for getting token using person ID for a person who had logged in the app for a week
def get_token_person_id(person_id): 
    if token.objects.filter(person_id=person_id).exists():
        tokens = token.objects.get(person_id=person_id) 
        day = datetime.datetime.now(tz=pytz.UTC) - tokens.created_at
        if day.seconds <= 7*24*3600:
            return tokens.token
        elif day.seconds > 7*24*3600:
            tokens.delete()
            create = create_token_person_id(person_id=person_id)
            return create.token
    else:
        create = create_token_person_id(person_id=person_id)
        return create.token



# function for getting token using unique device ID for a person who had logged in the app for a week
def get_token_device_id(device_id):
    if token.objects.filter(device_id=device_id).exists(): 
        tokens = token.objects.get(device_id=device_id)
        day = datetime.datetime.now(tz=pytz.UTC) - tokens.created_at
        if day.seconds <= 7*24*3600:
            return tokens.token
        elif day.seconds > 7*24*3600:
            tokens.delete()
            create = create_token_device_id(device_id=device_id)
            return create.token
    else:
        create = create_token_device_id(device_id=device_id)
        return create.token


def token_eligibilty(tokenation):
    if token.objects.filter(token=tokenation).exists():
        tokens = token.objects.get(token=tokenation)
        day = datetime.datetime.now(tz=pytz.UTC) - tokens.created_at
        if day.seconds <= 7*24*3600:
            return True
        elif day.seconds > 7*24*3600:
            return False
    else:
        return False
