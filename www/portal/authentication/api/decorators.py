from authentication.api import views
from rest_framework.response import Response
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect


def check_token_status():
    def decorator(view_func):
        def wrap(request, *args, **kwargs):
            if request.headers['token'] is not None:
                eligible = views.token_eligibilty(tokenation=request.headers['token'])
                if eligible:
                    return view_func(request, *args, **kwargs)
                else:
                    success={'message':'token expired','status_code':401} 
                    return Response(success)
            else:
                success={'message':'No token provided','status_code':402} 
                return Response(success)
        return wrap
    return decorator
