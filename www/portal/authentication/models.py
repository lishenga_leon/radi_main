from django.db import models

# Create your models here.

class token(models.Model):

    id = models.AutoField(primary_key=True)
    token = models.CharField(max_length=10000, default=None)
    person_id = models.CharField(max_length=255, default='NOT APPLICABLE')
    device_id = models.CharField(max_length=255, default='NOT APPLICABLE')
    created_at = models.DateTimeField(default=None)
