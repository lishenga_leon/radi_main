import datetime

JWT_AUTH = {
    'JWT_ENCODE_HANDLER':
    'rest_framework_jwt.utils.jwt_encode_handler',

    'JWT_DECODE_HANDLER':
    'rest_framework_jwt.utils.jwt_decode_handler',

    'JWT_PAYLOAD_HANDLER':
    'rest_framework_jwt.utils.jwt_payload_handler',

    'JWT_PAYLOAD_GET_USER_ID_HANDLER':
    'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',

    'JWT_RESPONSE_PAYLOAD_HANDLER':
    #'rest_framework_jwt.utils.jwt_response_payload_handler',
    'accounts.api.utils.jwt_response_payload_handler',

    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),

    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    'JWT_AUTH_COOKIE': None,

}

PAYMENTSERVICE = 'http://209.97.136.165:8008'

HEADERS = {
    'Content-Type': "application/x-www-form-urlencoded",
    'cache-control': "no-cache",
}

FIREBASE_SERVER_KEY = 'AAAABupAxO8:APA91bG-RAcmkPH2mTPQsTxTRBZG5Dm4jNeiNx-RR57eurBoLYVUI8-ibM-rtuz7zvE7OrN3Jkef3rFGUGtelduPLrabps5UnVPjrh9PshgoBAFlbb1YrGVBAy2H3hh0uyjof5w39IWc'

FIREBASE_LEGACY_KEY = 'AIzaSyCVCgNcET5LSgH8Pubt4JxCxXLUo7AA9MM'

FIREBASE_SENDER_ID = '29699917039'

SENDGRID_API_KEY = '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'


PAYPAL_MODE='sandbox'   # sandbox or live
PAYPAL_CLIENT_ID='AbuwUaKZbGSEB2KYFUuPGnzlW3xt19b2oXkJfgcLhtmhYoHjKbrjJGY36aLcLlyfNC1FH-9qEjl6U9ZE'
PAYPAL_CLIENT_SECRET='EDfHx-x9turVAkBXitDEiRg1vcAwULRxtf6x43KWZuLgF2eMegeN3WsRl2oePguzhAW-4MabPvNmp7Nc'

