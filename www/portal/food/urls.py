from django.urls import include, path
from . import controller

urlpatterns = [
    
    path('food/create/', controller.create_food),
    path('food/update/', controller.update_food),
    path('food/delete/', controller.delete_food),
    path('food/findById/', controller.find_food_by_id),
    path('food/getAll/', controller.get_all_foods),
    path('food/RestaurantMenus/', controller.findRestaurantMenus),
    path('food/MenuFoods/', controller.findMenusFoods),

    path('menu/create_menu/', controller.create_menu),
    path('menu/find_food_for_restaurant_and_menu_by_id', controller.find_food_for_restaurant_and_menu_by_id),
    path('menu/find_menu_by_id', controller.find_menu_by_id),
    
    #Restaurant routes
    path('', include('restaurant.urls')),
]
