import requests
from food.helpers import helper
from django.core.paginator import Paginator
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Foodie, Menu, MenuFoodPivot
from authentication.api.decorators import check_token_status
import datetime
import pytz


@api_view(['POST'])
@check_token_status()
def create_food(request):
    """
    create food
    -----
        {
           name:string,
           price:float,
           restaurant_id:int,
           picture: hvdhd.....(base64)
           picture_encoding:png
           description: fjlajksa,
           menu_id: int
        }

    """
    try:
        decoder = helper.base64decoding(
            pictures=request.data['picture'],
            file_type=request.data['picture_encoding']
        )
        foo = Foodie(
            name=request.data["name"],
            price=request.data["price"],
            cover_image=decoder['newName'],
            description=request.data['description'],
            menu_id=request.data['menu_id'],
            restaurant_id=request.data["restaurant_id"],
            created_at=datetime.datetime.now(tz=pytz.UTC),
            updated_at=datetime.datetime.now(tz=pytz.UTC)
        )
        foo.save()
        data = {
            "message": 'updated',
            "status_code": 200
        }
        return Response(data)

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
    return Response(data)


@api_view(['POST'])
@check_token_status()
def create_menu(request):
    """
    create food
    -----
        {
           name:string,
           restaurant_id:int,
        }

    """
    try:
        menus = Menu(
            name=request.data["name"],
            restaurant_id=request.data["restaurant_id"],
            status=1,
            created_at=datetime.datetime.now(tz=pytz.UTC),
            updated_at=datetime.datetime.now(tz=pytz.UTC)
        )
        menus.save()
        data = {
            "message": 'success',
            "status_code": 200
        }

        return Response(data)

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
        return Response(data)


@api_view(['POST'])
@check_token_status()
def find_food_for_restaurant_and_menu_by_id(request):
    """
    -----
        {
           restaurant_id:int,
        }

    """
    try:
        menus = Menu.objects.filter(
            restaurant_id=request.data["restaurant_id"])
        value = []
        for men in menus:
            details = {
                'id': men.id,
                'name': men.name,
                'restaurant_id': men.restaurant_id,
                'created_at': men.created_at,
                'updated_at': men.updated_at
            }
            value.append(details)

        det = []
        for cats in value:
            foo = Foodie.objects.filter(menu_id=cats['id'])
            for fods in foo:
                val = {
                    'restaurant_id': cats['restaurant_id'],
                    'food_id': fods.id,
                    'food_description': fods.description,
                    'menu_id': cats['id'],
                    'image': '/media/food/picture/'+fods.cover_image,
                    'price': fods.price,
                    'menu_name': cats['name'],
                    'created_at': fods.created_at,
                    'updated_at': fods.updated_at
                }
                det.append(val)
        data = {
            "data": det,
            "message": 'deleted',
            "status_code": 200
        }

        return Response(data)

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
        return Response(data)


@api_view(['POST'])
@check_token_status()
def findRestaurantMenus(request):
    """
    -----
        {
           restaurant_id:int,
        }

    """
    try:
        menus = Menu.objects.filter(
            restaurant_id=request.data["restaurant_id"])
        value = []
        for men in menus:
            details = {
                'id': men.id,
                'name': men.name,
                'status': men.status,
                'restaurant_id': men.restaurant_id,
                'created_at': men.created_at,
                'updated_at': men.updated_at
            }
            value.append(details)
        data = {
            "data": value,
            "message": 'deleted',
            "status_code": 200
        }

        return Response(data)

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
        return Response(data)


@api_view(['POST'])
@check_token_status()
def findMenusFoods(request):
    """
    -----
        {
           menu_id:int,
        }

    """
    try:
        foods = Foodie.objects.filter(menu_id=request.data["menu_id"])
        value = []
        for foo in foods:
            details = {
                'id': foo.id,
                'name': foo.name,
                'price': foo.price,
                'description': foo.description,
                'cover': '/media/food/picture/'+foo.cover_image,
                'created_at': foo.created_at,
                'updated_at': foo.updated_at
            }
            value.append(details)
        data = {
            "data": value,
            "message": 'deleted',
            "status_code": 200
        }

        return Response(data)

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
        print(data)
        return Response(data)


@api_view(['POST'])
@check_token_status()
def find_menu_by_id(request):
    """
    -----
        {
           restaurant_id:int,
        }

    """
    try:
        menus = Menu.objects.filter(
            restaurant_id=request.data["restaurant_id"])
        value = []
        for men in menus:
            details = {
                'id': men.id,
                'name': men.name,
                'restaurant_id': men.restaurant_id,
                'foods': men.foods(),
                'created_at': men.created_at,
                'updated_at': men.updated_at
            }
            value.append(details)
        data = {
            "data": value,
            "message": 'deleted',
            "status_code": 200
        }

        return Response(data)

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
        return Response(data)


@api_view(['PUT'])
@check_token_status()
def update_food(request):
    """
    update food
    -----
        {
           food_id:int,
           name:string,
           price:float,
           restaurant_id:int
        }

    """
    try:
        foo = Foodie.objects.get(id=request.data["food_id"])
        foo.name = request.data["name"]
        foo.price = request.data["price"]
        # foo.cover_image = "sample"
        foo.restaurant_id = request.data["restaurant_id"]
        data = {
            "data": {},
            "message": 'updated',
            "status_code": 200
        }

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
            'data': {

            }
        }
    return Response(data)


@api_view(['DELETE'])
@check_token_status()
def delete_food(request):
    """
    -----
        {
           food_id:int,
        }

    """
    try:
        foo = Foodie.objects.get(id=request.data["food_id"])
        foo.delete()
        data = {
            "data": {},
            "message": 'deleted',
            "status_code": 200
        }

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
            'data': {

            }
        }
    return Response(data)


@api_view(['POST'])
@check_token_status()
def get_all_foods(request):
    """
    -----
        {
           page:int,
           size:int,
           restaurant_id:int
        }

    """
    data = {}
    try:
        foods = Foodie.objects.get(
            restaurant_id__exact=request.data["restaurant_id"])
        page = request.GET.get('page', request.data['page'])
        paginator = Paginator(foods, request.data['size'])
        details = []
        for foo in paginator.page(page):
            values = {
                'name': foo.name,
                'price': foo.price,
                'cover_image': '/media/food/picture/'+foo.cover_image,
                'created_at': foo.created_at,
                'updated_at': foo.updated_at
            }

            details.append(values)

        data = {
            "data": details,
            "message": 'updated',
            "status_code": 200
        }

    except BaseException as e:

        data = {
            'status_code': 500,
            'message': 'error'+str(e),
            'data': {

            }
        }
    return Response(data)


@api_view(['GET'])
@check_token_status()
def find_food_by_id(request):
    """
    -----
        {
           food_id:int,
        }

    """
    try:
        foo = Foodie.objects.get(id=request.data["food_id"])
        details = {
            'name': foo.name,
            'price': foo.price,
            'cover_image': '/media/food/picture/'+foo.cover_image,
            'created_at': foo.created_at,
            'updated_at': foo.updated_at
        }
        data = {
            "data": details,
            "message": 'deleted',
            "status_code": 200
        }

    except BaseException as e:
        data = {
            'status_code': 500,
            'message': 'error'+str(e),
            'data': {

            }
        }
    return Response(data)
