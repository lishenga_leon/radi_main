from rest_framework.serializers import ModelSerializer
from .models import  Foodie


class FoodieSerializer(ModelSerializer):
    class Meta:
        model = Foodie
        fields = '__all__'