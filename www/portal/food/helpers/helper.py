import urllib.request
import string
import random
import os
import shutil
from PIL import Image
from django.core.files.storage import FileSystemStorage
from mimetypes import guess_extension
from urllib.request import urlretrieve
from django.conf import settings


def base64decoding(pictures, file_type):
    url = 'data:image/'+file_type+';base64,'+pictures
    filename, m = urlretrieve(url)
    size = 30
    chars = string.ascii_uppercase + string.digits
    newName = ''.join(random.choice(chars) for _ in range(size))
    newName_thumb = ''.join(random.choice(chars)
                            for _ in range(size))+'_thumb'+'.'+file_type
    shutil.copy(
        filename, settings.MEDIA_FOOD_PICTURE_ROOT+newName+'.'+file_type)

    data = {
        "newName": newName+'.'+file_type,
        "newName_thumb": newName_thumb,
    }

    return data


def find(Name):
    for root, dirs, files in os.walk(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))):
        if Name in files:
            return os.path.join(root, Name)
