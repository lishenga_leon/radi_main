from django.db import models
import datetime


# Create your models here.
class Foodie(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, default=None)
    price = models.FloatField(default=None)
    description = models.CharField(max_length=10000, default=None)
    menu_id = models.IntegerField(default=None)
    cover_image = models.CharField(max_length=255, default=None)
    restaurant_id = models.IntegerField(default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)



class Menu(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, default=None)
    restaurant_id = models.IntegerField(default=None)
    status = models.CharField(max_length=255, default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)

    def foods(self):
        foods = Foodie.objects.filter(menu_id=self.id)
        values = []
        for food in foods:
            details = {
                'id': food.id,
                'name': food.name,
                'price': food.price,
                'description': food.description,
                'cover': '/media/food/picture/'+food.cover_image,
                'created_at': food.created_at,
                'updated_at': food.updated_at
            }
            values.append(details)
        return values


class MenuFoodPivot(models.Model):
    id = models.AutoField(primary_key=True)
    menu_id = models.IntegerField(default=None)
    food_id = models.IntegerField(default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)


class ActiveMenu(models.Model):
    id = models.AutoField(primary_key=True)
    restaurant_id = models.IntegerField()
    menu_id = models.IntegerField(default=None)
    created_at = models.DateTimeField(default=None)
    updated_at = models.DateTimeField(default=None)
